package team237;

import battlecode.common.*;

public class RobotPlayer {

	public static void run(RobotController rc) {

		Bot robot = null;
		RobotType type = rc.getType();
		switch(type) {
		case ARCHON:
			robot = new Archon(rc);
			break;
		case SOLDIER:
			robot = new Soldier(rc);
			break;
		case GUARD:
			robot = new Guard(rc);
			break;
		case VIPER:
			robot = new Viper(rc);
			break;
		case TURRET:
			robot = new Turret(rc);
			break;
		default:
			break;
		}
		
		if (type == RobotType.SCOUT)
		{
			Signal[] signals = rc.emptySignalQueue();
			for (Signal signal : signals)
			{
				if (signal.getMessage() == null || signal.getTeam() != rc.getTeam())
					continue;
				switch (signal.getMessage()[0])
				{
				case Constants.SCOUT_EXPLORER:
					robot = new ExplorerScout(rc, signals);
					break;
				case Constants.SCOUT_ZOMBIE_COWBOY:
					robot = new ZombieCowboyScout(rc, signals);
					break;
				case Constants.SCOUT_SIGNALER:
					robot = new SignalerScout(rc, signals);
					break;
				}
				if (robot != null) {
					break;
				}
			}
			
			if (robot == null)
			{
				System.out.println("Scout was not given initialization code upon spawn. Defaulting to ExplorerScout.");
				System.out.println("Type requested: " + type);
				robot = new ExplorerScout(rc, signals);
			}
		}
		
		robot.run();
	}


}
