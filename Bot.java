package team237;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class Bot {
	protected RobotController rc;
	protected Random rand;
	protected Astar pathFinder;

	public double currentHealth;
	public RobotType type;
	public int leaderID = -1;
	public Team ourTeam;
	public Team oppTeam;

	public MapLocation leaderLoc;

	public enum State {
		ATTACK_ZOMBIEDEN, ATTACK_ENEMY_ARCHON, FOLLOW_LEADER,
		COLLECT_PARTS, COMBAT, FLEE, RETURN_TO_ARCHON, EXPLORE,
		DEFAULT, ZOMBIE_HERDER, EXPLORE_CHUNK, ZOMBIE_NIGHT_PREPARE;
	}
	public State state;
	public int numArchons;
	public int maxSignalRange;

	public MapBounds bounds = new MapBounds();

	public class PartPile {
		public double numParts;
		public double numRubble;
		public MapLocation location;
		public PartPile(double numParts, double numRubble, MapLocation location) {
			this.numParts = numParts;
			this.numRubble = numRubble;
			this.location = location;
		}
	}
	
	public class SightedRobot {
		public RobotInfo info;
		public int timestamp;
		public SightedRobot(RobotInfo info, int timestamp) {
			this.info = info;
			this.timestamp = timestamp;
		}
	}
	
	//Returns the better part pile
	public class PartPileComparator implements Comparator<PartPile> {
		@Override
		public int compare(PartPile o1, PartPile o2) {
			double score1 = (o1.numParts * Constants.PARTS_WEIGHT - numTurnsRequiredToClear(o1.numRubble));
			double score2 = (o1.numParts * Constants.PARTS_WEIGHT - numTurnsRequiredToClear(o1.numRubble));
			return (int)(score2 - score1); //Note: we want to return the max
		}
	}
	
	public List<Double> rubbleClearTurns = new ArrayList<Double>();

	
	public Bot(RobotController rcont) {
		rc = rcont;
		currentHealth = rc.getHealth();
		rand = new Random(rc.getID());
		type = rc.getType();
		maxSignalRange = (GameConstants.MAP_MAX_HEIGHT - 1)*(GameConstants.MAP_MAX_HEIGHT - 1)
				+ (GameConstants.MAP_MAX_WIDTH - 1)*(GameConstants.MAP_MAX_WIDTH - 1);
		leaderLoc = rc.getLocation();
		double rubble = GameConstants.RUBBLE_SLOW_THRESH;
		for (int i = 0; i < 20; ++i) {
			rubbleClearTurns.add(rubble);
			rubble = (rubble+GameConstants.RUBBLE_CLEAR_FLAT_AMOUNT)*(100/(100-GameConstants.RUBBLE_CLEAR_PERCENTAGE));
		}
		ourTeam = rcont.getTeam();
		oppTeam = ourTeam.opponent();
		pathFinder = new Astar(rc, bounds);
	}

	public void decrowd() throws GameActionException{
		// move around if crowded
		RobotInfo[] friendlies = rc.senseNearbyRobots(2, rc.getTeam());
		if (friendlies.length > 2) {
			if (rc.getType() == RobotType.TURRET) {
				rc.pack();
			} else{
				MapLocation l = rc.getLocation();
				for (RobotInfo bot : friendlies) {
					l.add(l.directionTo(bot.location));
				}
				Direction dir = rc.getLocation().directionTo(l);
				if (dir != Direction.OMNI && dir != Direction.NONE) {
					rc.move(l.directionTo(rc.getLocation()));
				} else {
					moveRandomly();
				}
			}
		}
		
	}

	public void moveRandomly() throws GameActionException {
		Direction dir = Constants.DIRECTIONS[rand.nextInt(Constants.DIRECTIONS.length)];
		for (int i = 0; i < 8; ++i) {
			if (rc.canMove(dir)) {
				moveToward(rc.getLocation().add(dir));
				return;
			}
			dir.rotateRight();
		}
		moveToward(rc.getLocation().add(dir));
	}

	public void run() {
	}

	/** Attacks the enemy with the lowest health **/
	public void attackEnemy(RobotInfo[] enemies) throws GameActionException {
		rc.attackLocation(lowestHealth(enemies).location);
	}
	
	public RobotInfo lowestHealth(List<RobotInfo> robots) {
		double minHealth = Double.MAX_VALUE;
		int target = 0;
		for (int i=0; i < robots.size(); ++i) {
			if (robots.get(i).health < minHealth) {
				minHealth = robots.get(i).health;
				target = i;
			}
		}
		return robots.get(target);
	}

	public RobotInfo lowestHealth(RobotInfo[] robots) {
		double minHealth = Double.MAX_VALUE;
		int target = 0;
		for (int i=0; i < robots.length; ++i) {
			if (robots[i].health < minHealth) {
				minHealth = robots[i].health;
				target = i;
			}
		}
		return robots[target];
	}

	public void moveToward(Direction dir) throws GameActionException {
		MapLocation myLoc = rc.getLocation();
		if (rc.canMove(dir)) {
			rc.move(dir);
		} else if (dir != Direction.OMNI) {
			Direction leftDir = dir.rotateLeft();
			if (rc.canMove(leftDir)) {
				rc.move(leftDir);
			} else {
				Direction rightDir = dir.rotateRight();
				if (rc.canMove(rightDir)) {
					rc.move(rightDir);
				} else if (!rc.getType().ignoresRubble && rc.onTheMap(rc.getLocation().add(dir))){
					double minRubble = Double.MAX_VALUE;
					Direction toClear = Direction.OMNI;
					double rRubble = rc.senseRubble(myLoc.add(rightDir));
					if (rRubble > 0 && rRubble < minRubble) {
						minRubble = rRubble;
						toClear = rightDir;
					}
					double lRubble = rc.senseRubble(myLoc.add(leftDir));
					if (lRubble > 0 && lRubble < minRubble) {
						minRubble = lRubble;
						toClear = leftDir;
					}
					double nRubble = rc.senseRubble(myLoc.add(dir));
					if (nRubble > 0 && nRubble < minRubble) {
						minRubble = nRubble;
						toClear = dir;
					}
					if (toClear != Direction.OMNI) {
						// Cheap fix for TTMs not being able to clear rubble
						if (rc.getType() != RobotType.TURRET && rc.getType() != RobotType.TTM)
							rc.clearRubble(toClear);
					}
				}
			}
		}
	}
	
	public void moveToward(MapLocation loc) throws GameActionException {
		Direction dir = rc.getLocation().directionTo(loc);
		moveToward(dir);
	}

	/** Allow important unit to retreat **/
	public void moveFrom(MapLocation loc) throws GameActionException {
		MapLocation myLoc = rc.getLocation();
		Direction backDir = loc.directionTo(myLoc);
		Direction backLeftDir = backDir.rotateLeft();
		Direction backRightDir = backDir.rotateRight();
		Direction leftDir = backLeftDir.rotateLeft();
		Direction rightDir = backRightDir.rotateRight();
		if (rc.canMove(backDir)) {
			rc.move(backDir);
		} else if (rc.canMove(backLeftDir)) {
			rc.move(backLeftDir);
		} else if (rc.canMove(backRightDir)) {
			rc.move(backRightDir);
		} else if (rc.canMove(leftDir)) {
			rc.move(leftDir);
		} else if (rc.canMove(rightDir)) {
			rc.move(rightDir);
		} else if (rc.onTheMap(myLoc.add(backDir))) {
			// Cheap fix for TTMs not being able to clear rubble
			if (rc.getType() != RobotType.TURRET && rc.getType() != RobotType.TTM) {
				rc.clearRubble(backDir);
			}
		}
	}

	public RobotInfo dangerousEnemy () {
		MapLocation myLoc = rc.getLocation();
		RobotInfo[] enemies = rc.senseHostileRobots(myLoc, type.sensorRadiusSquared);
		RobotInfo mostDangerous = null;
		double mostAttackPower = 0;
		if (enemies.length > 0) {
			for (RobotInfo enemy : enemies) {
				if (enemy.location.distanceSquaredTo(myLoc) <= enemy.type.attackRadiusSquared) {
					if (enemy.attackPower > mostAttackPower) {
						mostDangerous = enemy;
						mostAttackPower = enemy.attackPower;
					}
				}
			}
		}
		return mostDangerous;
	}

	public boolean underAttack() {
		if (rc.getHealth() < currentHealth) {
			currentHealth = rc.getHealth();
			return true;
		}
		return false;
	}

	/** Hashes a map location to an integer **/
	public int encodePosition(MapLocation position) {
		return Bot.encodePosition(position, rc);
	}
	
	/** Static version **/
	public static int encodePosition(MapLocation position, RobotController rc) {
		int dx = position.x - rc.getLocation().x;
		int dy = position.y - rc.getLocation().y;
		int pdx = GameConstants.MAP_MAX_WIDTH + dx; 
		int pdy = GameConstants.MAP_MAX_HEIGHT + dy; 
		return (2*GameConstants.MAP_MAX_HEIGHT + 1) * pdx + pdy;
	}

	/** Decodes the map location **/
	public static MapLocation decodePosition(MapLocation signalOrigin, int signal) {
		int pdx = signal / (2*GameConstants.MAP_MAX_HEIGHT + 1);
		int pdy = signal % (2*GameConstants.MAP_MAX_HEIGHT + 1);
		int dx = pdx - GameConstants.MAP_MAX_WIDTH; 
		int dy = pdy - GameConstants.MAP_MAX_HEIGHT; 
		return signalOrigin.add(dx, dy);
	}
	
	public int numTurnsRequiredToClear (double rubble) {
		for(int i = 0; i < rubbleClearTurns.size(); i++) {
			if (rubble < rubbleClearTurns.get(i)) {
				return i;
			}
		}
		double highestRubble = rubbleClearTurns.get(rubbleClearTurns.size()-1);
		return (int)(rubbleClearTurns.size() + (rubble - highestRubble) / 
				(GameConstants.RUBBLE_CLEAR_PERCENTAGE*highestRubble + GameConstants.RUBBLE_CLEAR_FLAT_AMOUNT));
	}
	
	public RobotInfo closest(List<RobotInfo> robots) {
		double minDistance = Double.MAX_VALUE;
		RobotInfo nearest = null;
		for (int i=0; i < robots.size(); ++i) {
			int distance = rc.getLocation().distanceSquaredTo(robots.get(i).location);
			if (distance < minDistance) {
				minDistance = distance;
				nearest = robots.get(i);
			}
		}
		return nearest;
	}
	
	public RobotInfo closest(RobotInfo[] robots) {
		double minDistance = Double.MAX_VALUE;
		RobotInfo nearest = null;
		for (int i=0; i < robots.length; ++i) {
			int distance = rc.getLocation().distanceSquaredTo(robots[i].location);
			if (distance < minDistance) {
				minDistance = distance;
				nearest = robots[i];
			}
		}
		return nearest;
	}
	
	public RobotInfo closest(RobotInfo[] robots, MapLocation loc) {
		double minDistance = Double.MAX_VALUE;
		RobotInfo nearest = null;
		for (int i=0; i < robots.length; ++i) {
			int distance = loc.distanceSquaredTo(robots[i].location);
			if (distance < minDistance) {
				minDistance = distance;
				nearest = robots[i];
			}
		}
		return nearest;
	}
	
	public MapLocation closest(Collection<MapLocation> locations) {
		int closest = Integer.MAX_VALUE;
		MapLocation closestLocation = null;
		for (MapLocation location : locations) {
			int distance = rc.getLocation().distanceSquaredTo(location);
			if (distance < closest) {
				closest = distance;
				closestLocation = location;
			}
		}
		return closestLocation;
	}
	
	public MapLocation closest(Collection<MapLocation> locations, MapLocation loc) {
		int closest = Integer.MAX_VALUE;
		MapLocation closestLocation = null;
		for (MapLocation location : locations) {
			int distance = loc.distanceSquaredTo(location);
			if (distance < closest) {
				closest = distance;
				closestLocation = location;
			}
		}
		return closestLocation;
	}
	
	public TimestampedMapLocation closest(Collection<TimestampedMapLocation> locations, TimestampedMapLocation loc) {
		int closest = Integer.MAX_VALUE;
		TimestampedMapLocation closestLocation = null;
		for (TimestampedMapLocation location : locations) {	
			int distance = loc.mapLocation.distanceSquaredTo(location.mapLocation);
			if (distance < closest) {
				closest = distance;
				closestLocation = location;
			}
		}
		return closestLocation;
	}
	
	protected List<RobotInfo> filterBots(List<RobotInfo> robots, RobotType type) {
		List<RobotInfo> filtered = new ArrayList<RobotInfo>();
		for (RobotInfo robot : robots) {
			if (robot.type == type)
				filtered.add(robot);
		}
		return filtered;
	}
	
	protected RobotInfo[] filterBots(RobotInfo[] robots, RobotType type) {
		List<RobotInfo> filtered = new ArrayList<RobotInfo>();
		for (RobotInfo robot : robots) {
			if (robot.type == type)
				filtered.add(robot);
		}
		return (RobotInfo[]) filtered.toArray();
	}
	
	protected List<RobotInfo> filterBots(List<RobotInfo> robots, Team team, boolean exclude) {
		List<RobotInfo> filtered = new ArrayList<RobotInfo>();
		for (RobotInfo robot : robots) {
			if ((robot.team == team && !exclude) || (robot.team != team && exclude))
				filtered.add(robot);
		}
		return filtered;
	}
	
	protected RobotInfo[] filterBots(RobotInfo[] robots, Team team, boolean exclude) {
		List<RobotInfo> filtered = new ArrayList<RobotInfo>();
		for (RobotInfo robot : robots) {
			if ((robot.team == team && !exclude) || (robot.team != team && exclude))
				filtered.add(robot);
		}
		return (RobotInfo[]) filtered.toArray();
	}
	
	protected List<RobotInfo> filterBots(List<RobotInfo> robots, Team team) {
		List<RobotInfo> filtered = new ArrayList<RobotInfo>();
		for (RobotInfo robot : robots) {
			if (robot.team == team)
				filtered.add(robot);
		}
		return filtered;
	}
	
	protected RobotInfo[] filterBots(RobotInfo[] robots, Team team) {
		List<RobotInfo> filtered = new ArrayList<RobotInfo>();
		for (RobotInfo robot : robots) {
			if (robot.team == team)
				filtered.add(robot);
		}
		return (RobotInfo[]) filtered.toArray();
	}
	
	protected void upkeep() throws GameActionException {
		
	}
	
	public Direction directionTo(double dx, double dy) {
		if (Math.abs(dx) >= 2.414 * Math.abs(dy)) {
			if (dx > 0) {
				return Direction.EAST;
			} else if (dx < 0) {
				return Direction.WEST;
			} else {
				return Direction.OMNI;
			}
		} else if (Math.abs(dy) >= 2.414 * Math.abs(dx)) {
			if (dy > 0) {
				return Direction.SOUTH;
			} else {
				return Direction.NORTH;
			}
		} else {
			if (dy > 0) {
				if (dx > 0) {
					return Direction.SOUTH_EAST;
				} else {
					return Direction.SOUTH_WEST;
				}
			} else {
				if (dx > 0) {
					return Direction.NORTH_EAST;
				} else {
					return Direction.NORTH_WEST;
				}
			}
		}		
	}

	public int getMaxSignalRange() {
		MapLocation loc = rc.getLocation();
		if (!bounds.allBoundsKnown()) {
			return maxSignalRange;
		} else {
			int dx = Math.max(loc.x - bounds.mapLeft, bounds.mapRight - loc.x);
			int dy = Math.max(loc.y - bounds.mapTop, bounds.mapBottom - loc.y);
			return dx * dx + dy * dy;
		}
	}

	/** Code for sending transmissions 
	 * @throws GameActionException **/
	protected void sendTransmission(int targetID, int radius, int... data) throws GameActionException {
		rc.broadcastMessageSignal(Constants.TRANSMISSION_START, targetID, radius);
		
		for (int i = 0; i < data.length; i += 2)
		{
			int part1 = data[i];
			int part2 = (i + 1 < data.length) ? data[i + 1] : 0; 
			rc.broadcastMessageSignal(part1, part2, radius);
		}
		
		rc.broadcastMessageSignal(Constants.TRANSMISSION_END, targetID, radius);
	}

	protected int[][] allocateChunks() {
		int mapWidth = bounds.mapRight - bounds.mapLeft + 1;
		int mapHeight = bounds.mapBottom - bounds.mapTop + 1;
		int chunkRowSize = mapWidth / Constants.SCOUT_CHUNK_SIZE;
		int chunkColumnSize = mapHeight / Constants.SCOUT_CHUNK_SIZE;
		// handle partial chunks
		if (mapWidth % Constants.SCOUT_CHUNK_SIZE != 0) {
			chunkRowSize += 1;
		}
		if (mapHeight % Constants.SCOUT_CHUNK_SIZE != 0) {
			chunkColumnSize += 1;
		}
		return new int[chunkRowSize][chunkColumnSize];
	}

	protected int[] getChunk(MapLocation loc) {
		int[] chunk = {
			(loc.x - bounds.mapLeft) / Constants.SCOUT_CHUNK_SIZE,
			(loc.y - bounds.mapTop) / Constants.SCOUT_CHUNK_SIZE
		};
		return chunk;
	}

	protected MapLocation getTopLeft(int[] chunk) {
		return new MapLocation(
				chunk[0] * Constants.SCOUT_CHUNK_SIZE + bounds.mapLeft,
				chunk[1] * Constants.SCOUT_CHUNK_SIZE + bounds.mapTop);
	}

	protected MapLocation getCenter(int[] chunk) {
		return getTopLeft(chunk).add(
			Constants.SCOUT_CHUNK_SIZE / 2, Constants.SCOUT_CHUNK_SIZE / 2);
	}

	protected boolean canSee(int[] chunk) {
		MapLocation topLeft = getTopLeft(chunk);
		MapLocation topRight = topLeft.add(Constants.SCOUT_CHUNK_SIZE - 1, 0);
		MapLocation bottomLeft = topLeft.add(0, Constants.SCOUT_CHUNK_SIZE - 1);
		MapLocation bottomRight = topLeft.add(
				Constants.SCOUT_CHUNK_SIZE - 1, Constants.SCOUT_CHUNK_SIZE - 1);
		return (rc.canSense(topLeft) && rc.canSense(topRight)
				&& rc.canSense(bottomLeft) && rc.canSense(bottomRight));
	}

	protected int getNumPartsOnChunk(int[] chunk) throws GameActionException {
		MapLocation topLeft = getTopLeft(chunk);
		int parts = 0;
		for (int x = 0; x < Constants.SCOUT_CHUNK_SIZE; x++) {
			for (int y = 0; y < Constants.SCOUT_CHUNK_SIZE; y++) {
				MapLocation loc = topLeft.add(x, y);
				parts += rc.senseParts(loc);
			}
		}
		for (RobotInfo rob : rc.senseNearbyRobots(-1, Team.NEUTRAL)) {
			if (topLeft.x <= rob.location.x && rob.location.x <= topLeft.x + Constants.SCOUT_CHUNK_SIZE) {
				if (topLeft.y <= rob.location.y && rob.location.y <= topLeft.y + Constants.SCOUT_CHUNK_SIZE) {
					if (rob.type.equals(RobotType.ARCHON)) {
						// neutral archons are valuable!
						parts += Constants.NEUTRAL_ARCHON_VALUE;
					} else {
						parts += rob.type.partCost;
					}
				}
			}
		}
		return parts;
	}

	protected void cleanPriorityQueue(PriorityQueue<TimestampedMapLocation> queue) {
		Iterator<TimestampedMapLocation> it = queue.iterator();
		while (it.hasNext())
		{
			if (rc.getRoundNum() - it.next().roundNumber > Constants.ENEMY_ARCHON_TIMESTAMP_STALENESS)
				it.remove();
		}
	}

	protected void cleanNearbyRubble() throws GameActionException{
		double lowestRubble = Integer.MAX_VALUE;;
		Direction lowestRubbleDir = null;
		Direction dir = Constants.DIRECTIONS[rand.nextInt(Constants.DIRECTIONS.length)];
		for (int i = 0; i < 8; ++i) {
			MapLocation loc = rc.getLocation().add(dir);
			double rubble = rc.senseRubble(loc);
			if (rubble > 0 && rubble < lowestRubble) {
				lowestRubble = rubble;
				lowestRubbleDir = dir;
			}
			dir.rotateRight();
		}
		if (lowestRubbleDir != null) {
			rc.clearRubble(lowestRubbleDir);
		}
	}

}
