package team237;

import battlecode.common.Direction;
import battlecode.common.RobotType;

public final class Constants {
    public final static Direction[] DIRECTIONS = {Direction.NORTH, Direction.NORTH_EAST,
    		Direction.EAST, Direction.SOUTH_EAST, Direction.SOUTH,
    		Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST};
    
    public enum SpecializedRobotType {
    	Archon, ExplorerScout, ZombieCowboyScout, SignalerScout, Soldier, Guard, Viper, Turret;
    	
    	public static RobotType getRobotType(SpecializedRobotType type) {
    		switch (type) {
    		case Archon:
    			return RobotType.ARCHON;
    		case ExplorerScout:
    			return RobotType.SCOUT;
    		case ZombieCowboyScout:
    			return RobotType.SCOUT;
    		case SignalerScout:
    			return RobotType.SCOUT;
    		case Soldier:
    			return RobotType.SOLDIER;
    		case Guard:
    			return RobotType.GUARD;
    		case Viper:
    			return RobotType.VIPER;
    		case Turret:
    			return RobotType.TURRET;
    		default:
    			return null;
    		}
    	}
    }
    
    // Spawn distributions. Must add up to 1.0
	// 															Archon	EScout	ZCScout	SScout	Soldier	Guard	Viper	Turret
    public final static double[] STARTING_GAME_DEFAULT_SPAWN = {0.0,	0.05,	0.0,	0.0,	0.95,	0.0,	0.0,	0.0};
    public final static double[] EARLY_GAME_DEFAULT_SPAWN = {	0.0,	0.05,	0.0,	0.0,	0.95,	0.0,	0.0, 	0.0};
	public final static double[] MID_GAME_DEFAULT_SPAWN = {		0.0,	0.05,	0.0,	0.0,	0.95,	0.0,	0.0,	0.0};
	public final static double[] LATE_GAME_DEFAULT_SPAWN = {	0.0,	0.05,	0.0,	0.0,	0.95,	0.0,	0.0,	0.0};
	public final static double[] MID_GAME_EXPLORE_MORE_SPAWN = {0.0,	0.05,	0.0,	0.0,	0.95,	0.0,	0.0,	0.0};
	
	public final static double[] STARTING_GAME_MANY_ZOMBIES_SPAWN = {0.0,	0.0,	0.0,	0.0,	0.9,	0.1};
	public final static double[] EARLY_GAME_MANY_ZOMBIES_SPAWN = {	0.0,	0.0,	0.0,	0.0,	0.9,	0.1};
	public final static double[] MID_GAME_MANY_ZOMBIES_SPAWN = {0.0,	0.0,	0.0,	0.0,	0.9,	0.1,	0.0,	0.0};
	public final static double[] LATE_GAME_MANY_ZOMBIES_SPAWN = {	0.0,	0.0,	0.0,	0.0,	0.9,	0.1,	0.0,	0.0};
	
	public final static int EARLY_GAME_CUTOFF = 500;
	public final static int LATE_GAME_CUTOFF = 1000;
	public final static int VERY_LATE_GAME_CUTOFF = 1700;
	
//	public final static RobotType[] UNIT_TYPE = {RobotType.ARCHON, RobotType.SOLDIER, 
//		RobotType.GUARD, RobotType.TURRET, RobotType.VIPER};
//	public final static int[] IDEAL_COUNT = {1, 5, 5, 1, 1};
//	public final static int TROOPS_NEEDED_TO_ATTACK_ZOMBIEDEN = 5;
//	public final static int TROOPS_NEEDED_TO_COLLECT_PARTS = 3;
	public final static int PART_STOCKPILE = 200;
	public final static int LATE_GAME_PARTS_STOCKPILE = 200;
	public final static int LATE_GAME_UNIT_PARTS_STOCKPILE = 120;
	
	// Scout types
	public final static int SCOUT_EXPLORER = 339641070;
	public final static int SCOUT_ZOMBIE_COWBOY = 410705472;
	public final static int SCOUT_SIGNALER = 705472374;

	public final static int TRANSMISSION_START = 647348095;
	public final static int TRANSMISSION_END = 952361128;
	public final static int SCOUT_SENSE_PARTS_TIME = 10;
	public final static int ASSIGN_SOLDIER_TO_SQUAD_CODE = 63915072;
	public final static int FORGET_ENEMY_ARCHON_THRESHOLD = 60;
	public final static int ZOMBIE_DEN_FOUND_CODE = 24553120;
	public final static int ENEMY_ARCHON_FOUND_CODE = 613233741;
	public final static int ATTACK_ZOMBIE_DEN_CODE = 1125400;
	public final static int RETREAT_CODE = 1125401;
	public final static int DRAFT_CODE = 669628558;
	public final static int FOLLOW_LEADER_CODE = 68772086;
	public final static int ZOMBIE_DEN_DESTROYED_CODE = 73260217;
	public final static int COLLECT_PARTS_CODE = 124813912;
	public final static int ENTER_COMBAT_CODE = 872753648;
	public final static int PARTS_HAS_BEEN_COLLECTED_CODE = 66495595;
	public final static int DEFAULT_CODE = 187972009;
	public final static int BOTTOM_MAP_BOUND_CODE = 1402879;
	public final static int TOP_MAP_BOUND_CODE = 874209;
	public final static int LEFT_MAP_BOUND_CODE = 1284097;
	public final static int RIGHT_MAP_BOUND_CODE = 9999999;
	
	public final static int ARCHON_ZOMBIE_FORECAST = 200;
	public final static int ARCHON_ZOMBIE_DANGER_SCORE = 300;
	public final static int ARCHON_EARLY_GAME_ZOMBIE_CAUTIOUSNESS = 6;
	
	public final static int DRAFT_ATTACK_SOLDIER_THRESHOLD = 10;
	public final static int DRAFT_ATTACK_VIPER_THRESHOLD = 3;
	public final static int DRAFT_TURNS_THRESHOLD = 100;
	public final static double DRAFT_ODDS = 0.5;
	
	public final static double VIPER_PRIORITY = 2.0;
	public final static double BIG_ZOMBIE_PRIORITY = 1.0;

	
	public final static double PERCENTAGE_MAP_EXPLORED_TO_NOT_EXPLORE_MORE = 0.5;
	
	/** How many enemy archon locations should a given zombie cowboy receive from the archon **/
	public final static int ZOMBIE_COWBOY_OBJECTIVES_NUM = 3;
	
	/** Number of turns before we delete stale objectives **/
	public final static int ENEMY_ARCHON_TIMESTAMP_STALENESS = 500;
	
	public final static int ARCHON_SQUAD_RADIUS_SQAURED = 300;
	
	/** A measure of how important parts are to us**/
	public final static double PARTS_WEIGHT = 0.1;
	/** Number of turns since seeing an enemy after which we declare combat is over **/
	public final static int NUM_TURNS_END_COMBAT = 10;
	/** Bodyguards will periodically signal when they find an enemy
	 *  during combat. If the archon doesn't receive any signals after
	 *  NUM_TURNS_END_COMBAT turns, the archon declares combat is over
	 */
	public final static int NUM_TURNS_SIGNAL_COMBAT = 10;
	public final static int RUN_AWAY_DISTANCE_SQAURED = 2;
	public final static double SCOUT_SAFE_HEALTH_PERCENTAGE = 0.80;
	public final static double SCOUT_DANGER_HEALTH_PERCENTAGE = 0.50;
	/** Percent below which the robot will suicide into
	 *  enemy archons if pursued by zombies.
	 */
	public final static double SCOUT_LOST_HEALTH_PERCENTAGE = 0.30;
	
	public final static double ZOMBIE_DEN_PRIORITY = 100;
	public final static double TROOP_VALUE = 0.75;
	public final static double PART_VALUE = 0.1;

	// Ideal distance for how close the turret should be to the scout
	public final static double TURRET_LEADER_CLOSENESS = 4;
	
	public final static double TURRET_INNER_RANGE = 5;
	
	public final static int VIPER_INFECT_HEALTH = 15;

	public final static int SCOUT_CHUNK_SIZE = 5;
	
	public final static int REROUTING_DISTANCE = 100;
	
	public final static int PART_SENSING_RADIUS = 12;

	public final static int NEUTRAL_ARCHON_VALUE = 3000;
	
	public final static int OVERRIDING_PRIORITY_VALUE = 2000;

	public final static int NEUTRAL_TURRET_VALUE = 10;

	public final static int NEUTRAL_UNIT_VALUE_WEIGHT = 1;

}
