package team237;

import java.util.ArrayList;
import java.util.List;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

class MapBounds {
	int mapTop = Integer.MIN_VALUE;
	int mapBottom = Integer.MAX_VALUE;
	int mapLeft = Integer.MIN_VALUE;
	int mapRight = Integer.MAX_VALUE;

	Direction update(RobotController rc) throws GameActionException{
		int sensorRadius = (int) Math.sqrt(rc.getType().sensorRadiusSquared);
		MapLocation myLoc = rc.getLocation();
		// scan for map edges in 4 cardinal directions
		// checking tiles farthest away from us first to
		// minimize bytecodes when not near the edge
		// left sweep
		if (mapLeft == Integer.MIN_VALUE) {
			MapLocation loc = myLoc.add(Direction.WEST, sensorRadius);
			if (!rc.onTheMap(loc)) {
				loc = loc.add(Direction.EAST);
				while (!rc.onTheMap(loc)){
					loc = loc.add(Direction.EAST);
				}
				mapLeft = loc.x;
				return Direction.WEST;
			}
		}
		// right sweep
		if (mapRight == Integer.MAX_VALUE) {
			MapLocation loc = myLoc.add(Direction.EAST, sensorRadius);
			if (!rc.onTheMap(loc)) {
				loc = loc.add(Direction.WEST);
				while (!rc.onTheMap(loc)){
					loc = loc.add(Direction.WEST);
				}
				mapRight = loc.x;
				return Direction.EAST;
			}
		}
		// north sweep
		if (mapTop == Integer.MIN_VALUE) {
			MapLocation loc = myLoc.add(Direction.NORTH, sensorRadius);
			if (!rc.onTheMap(loc)) {
				loc = loc.add(Direction.SOUTH);
				while (!rc.onTheMap(loc)){
					loc = loc.add(Direction.SOUTH);
				}
				mapTop = loc.y;
				return Direction.NORTH;
			}
		}
		// south sweep
		if (mapBottom == Integer.MAX_VALUE) {
			MapLocation loc = myLoc.add(Direction.SOUTH, sensorRadius);
			if (!rc.onTheMap(loc)) {
				loc = loc.add(Direction.NORTH);
				while (!rc.onTheMap(loc)){
					loc = loc.add(Direction.NORTH);
				}
				mapBottom = loc.y;
				return Direction.SOUTH;
			}
		}
		return Direction.NONE;
	}

	public boolean isInBounds(MapLocation loc) {
		return !(loc.x < mapLeft || loc.x > mapRight
			|| loc.y < mapTop || loc.y > mapBottom);
	}

	public boolean allBoundsKnown() {
		return (!(
				mapLeft == Integer.MIN_VALUE ||
				mapRight == Integer.MAX_VALUE ||
				mapTop == Integer.MIN_VALUE ||
				mapBottom == Integer.MAX_VALUE));
	}
	
	public List<MapLocation> getAllKnownCorners() {
		List<MapLocation> corners = new ArrayList<MapLocation>();
		if (mapLeft != Integer.MIN_VALUE && mapBottom != Integer.MAX_VALUE)
			corners.add(new MapLocation(mapLeft, mapBottom));
		if (mapBottom != Integer.MAX_VALUE && mapRight != Integer.MAX_VALUE)
			corners.add(new MapLocation(mapRight, mapBottom));
		if (mapRight != Integer.MAX_VALUE && mapTop != Integer.MIN_VALUE)
			corners.add(new MapLocation(mapRight, mapTop));
		if (mapTop != Integer.MIN_VALUE && mapLeft != Integer.MIN_VALUE)
			corners.add(new MapLocation(mapRight, mapTop));
		return corners;
	}

	public int getWidth() {
		return mapRight - mapLeft;
	}

	public int getHeight() {
		return mapBottom - mapTop;
	}
}
