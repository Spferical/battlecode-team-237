package team237;


import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Set;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

/** Pathfinding class implemented using A* 
 * 	
 * 	To use this class, first create an instance of this class.
 * 	Do this only once for each robot.
 * 	To move to a destination, first initialize the route. 
 * 	Do this only once for each destination.
 *  Then, run the methods calculateRoute() and moveAlongRoute() once per turn.
 *  This will cause the robot to find the shortest path to the destination.
 *  Finding the path may take several turns.
 *  Once the path is found, the robot will move along the path.
 *  Once the robot is at the destination, the robot will stop moving.
 *  
 *  **/

//TODO Deal with neutral robots found along path

public class Astar {
	private RobotController robot;
	private Set<MapLocation> closedSet;
	private PriorityQueue<Node> openSet;
	private Map<MapLocation, Double> obstacles;
	private Deque<Node> route;
	private Deque<Node> oldRoute;
	private Direction[] directions = {Direction.NORTH_WEST, Direction.NORTH_EAST, Direction.SOUTH_WEST, Direction.SOUTH_EAST};
	private MapLocation source;
	private MapLocation destination;
	private MapLocation lastLocation;
	public boolean isDestinationFound;
	private boolean isDestinationReached;
	private int turnsBlocked;
	private int numTurnsBeforeMove = 0;
	private Random rand;
	private List<Double> rubbleClearTurns;
	private int maxRubbleClearTurns = 10;
	/** Robots should prefer going through tiles it can see
	 * This amount is in units of rubble. */
	private double unexploredTilePenalty = 0.8;
	private MapBounds bounds;

	public Astar(RobotController robot, MapBounds bounds) {
		this.robot = robot;
		this.closedSet = new HashSet<MapLocation>();
		this.openSet = new PriorityQueue<Node>();
		this.obstacles = new HashMap<MapLocation, Double>();
		this.route = new ArrayDeque<Node>();
		this.oldRoute = new ArrayDeque<Node>();
		this.isDestinationFound = true;
		this.isDestinationReached = true;
		this.rand = new Random(robot.getID());
		this.rubbleClearTurns = new ArrayList<Double>();
		double rubble = GameConstants.RUBBLE_SLOW_THRESH;
		for (int i = 0; i < maxRubbleClearTurns+1; ++i) {
			rubbleClearTurns.add(rubble);
			rubble = (rubble+GameConstants.RUBBLE_CLEAR_FLAT_AMOUNT)*(100/(100-GameConstants.RUBBLE_CLEAR_PERCENTAGE));
		}
		this.bounds = bounds;
	}
	
	/** 
	 * @param destination
	 * This is the MapLocation to where the robot will attempt to path to.
	 * It is not checked whether it is possible to move to this destination.
	 * It is not checked whether this destination is on the map.
	 */
	public void initializeRoute(MapLocation destination) {
		closedSet.clear();
		route.clear();

		MapLocation source = robot.getLocation();
		NodeComparator comparator = new NodeComparator(destination);
		openSet = new PriorityQueue<Node>(10, comparator);

		Node src = new Node(source, Direction.NONE, null, 0, 0, rubbleClearTurns);
		openSet.add(src);
		
		{
			MapLocation loc = robot.getLocation().add(Direction.NORTH);
			if (robot.canSense(loc)) {
				double rubble = robot.senseRubble(loc);
				openSet.add(new Node(loc, Direction.NORTH, src, 1, rubble, rubbleClearTurns));
			}
		}
		{
			MapLocation loc = robot.getLocation().add(Direction.EAST);
			if (robot.canSense(loc)) {
				double rubble = robot.senseRubble(loc);
				openSet.add(new Node(loc, Direction.EAST, src, 1, rubble, rubbleClearTurns));
			}
		}
		{
			MapLocation loc = robot.getLocation().add(Direction.WEST);
			if (robot.canSense(loc)) {
				double rubble = robot.senseRubble(loc);
				openSet.add(new Node(loc, Direction.WEST, src, 1, rubble, rubbleClearTurns));
			}
		}
		{
			MapLocation loc = robot.getLocation().add(Direction.SOUTH);
			if (robot.canSense(loc)) {
				double rubble = robot.senseRubble(loc);
				openSet.add(new Node(loc, Direction.SOUTH, src, 1, rubble, rubbleClearTurns));
			}
		}

		this.destination = destination;
		this.source = source;
		this.isDestinationFound = false;
		this.isDestinationReached = false;
		this.lastLocation = source;
		this.turnsBlocked = 0;
	}

	/**
	 * If this robot has not found a path to the destination, this will do nothing.
	 * If the robot has already reached the destination, this will do nothing.
	 * Otherwise, the robot will move towards the destination.
	 * It is not checked whether the robot has core delay or not.
	 * If an obstacle is detected along the route, then the obstacle is stored
	 * and the route is recalculated.
	 * If the robot moves in a direction that is not along the route, the robot will
	 * abandon pathing along the route.
	 * @throws GameActionException
	 */
	public void moveAlongRoute() throws GameActionException {
		if (isDestinationFound && !isDestinationReached) {
			if (!lastLocation.equals(robot.getLocation())) {
				//Something happened. Reinitailize
				isDestinationFound = true;
				isDestinationReached = true;
				initializeRoute(destination);
			}
			Direction dir;
			if (!route.isEmpty()) {
				dir = route.peek().source;	
				MapLocation nextLoc = route.peek().location;
				double rubble = robot.senseRubble(nextLoc);
				if (robot.canMove(dir) && rubble < GameConstants.RUBBLE_SLOW_THRESH) {
					robot.move(dir);
					lastLocation = robot.getLocation();
					route.pop();
					turnsBlocked = 0;
					if (robot.getLocation().equals(destination)) {
						isDestinationReached = true;
						oldRoute.clear();
					}
				} else {
					if (rubble >= GameConstants.RUBBLE_SLOW_THRESH) {
						if (!obstacles.containsKey(nextLoc)) {
							obstacles.put(nextLoc, rubble);
						}
						if (!source.equals(robot.getLocation())) {
							oldRoute = new ArrayDeque<Node>(route);
							//robot.setIndicatorString(1, "REROUTING "+robot.getRoundNum()+" "+nextLoc+" "+oldRoute.peek().location);
							initializeRoute(destination);
						}
						//Clear rubble
						if (rubble >= GameConstants.RUBBLE_SLOW_THRESH) {
							Direction toRubble = robot.getLocation().directionTo(nextLoc);
							if (toRubble != Direction.OMNI) {
								
								// Cheap fix for TTMs not being able to clear rubble
								if (robot.getType() != RobotType.TURRET && robot.getType() != RobotType.TTM)
									robot.clearRubble(toRubble);
							}
						}
						if (rubble < GameConstants.RUBBLE_SLOW_THRESH && obstacles.containsKey(nextLoc)) {
							obstacles.remove(nextLoc);
						}
					}
					else if (!robot.canSense(nextLoc)) {
						obstacles.put(route.peek().location, 1000.0);
						initializeRoute(destination);
					}
					else if (!bounds.isInBounds(nextLoc)) {
						initializeRoute(destination);
					}
					else {
						//robot.setIndicatorString(1, "TURNS BLOCKED "+turnsBlocked + " "+robot.getRoundNum());
						if (robot.getLocation().distanceSquaredTo(destination) < Constants.REROUTING_DISTANCE) {
							turnsBlocked++;
							if (turnsBlocked >= numTurnsBeforeMove) {
								MapLocation loc = robot.getLocation();
								if(moveRandomly()) {
									oldRoute = new ArrayDeque<Node>(route);
									Direction source = robot.getLocation().directionTo(loc);
									oldRoute.push(new Node(loc, source, null, 0, 0, rubbleClearTurns));
									initializeRoute(destination);
								}
								return;
							}							
						}
						else {
							if (robot.isCoreReady()) {
								moveToward(robot.getLocation().directionTo(destination));
							}
						}
					}
				}
			}
			else {
				if (robot.getLocation().isAdjacentTo(destination)) {
					dir = robot.getLocation().directionTo(destination);
					if (robot.canMove(dir)) {
						robot.move(dir);
						isDestinationReached = true;
						oldRoute.clear();
					}
				} else {
					initializeRoute(destination);
				}
			}
		}
		else if (!oldRoute.isEmpty()) {
			//If you clear your rubble while calculating the new route, return to the original route.
			MapLocation loc = oldRoute.peek().location;
			//robot.setIndicatorString(2, "old route "+robot.getRoundNum()+" "+loc);
			if (robot.canSense(loc)) {
				double rubble = robot.senseRubble(loc);
				if (rubble >= GameConstants.RUBBLE_SLOW_THRESH) {
					Direction toRubble = robot.getLocation().directionTo(loc);
					if (toRubble != Direction.OMNI) {
						// Cheap fix for TTMs not being able to clear rubble
						if (robot.getType() != RobotType.TURRET && robot.getType() != RobotType.TTM)
							robot.clearRubble(toRubble);
					}
				}
				if (rubble < GameConstants.RUBBLE_SLOW_THRESH && obstacles.containsKey(loc)) {
					obstacles.remove(loc);
				}
				if (robot.canMove(robot.getLocation().directionTo(loc)) && rubble < GameConstants.RUBBLE_SLOW_THRESH) {
					isDestinationFound = true;
					route = new ArrayDeque<Node>(oldRoute);
					//robot.setIndicatorString(1, "Going back to old route "+robot.getRoundNum()+" "+loc);
				}
			}
		}
	}

	/**
	 * If the robot has found a route to the destination, this method will do nothing.
	 * Otherwise, the robot will attempt to find a path the destination.
	 * If the robot attempts to make a path through an obstacle it can see,
	 * that obstacle will be stored in memory.
	 * 
	 * @param maxEvaluations
	 * This parameter is the maximum number of nodes that will be explored this turn.
	 * For destinations that are far away, there is not enough bytecode to calculate
	 * the route in one turn. This allows you to calculate a portion of the route each
	 * turn.
	 */
	public void calculateRoute(int maxEvaluations) {
		if (!isDestinationFound) {
			if (source.isAdjacentTo(destination)) {
				isDestinationFound = true;
				oldRoute.clear();
				return;
			}
			int evaluations = 0;
			while (!openSet.isEmpty()  && evaluations < maxEvaluations) {	
				Node explore = openSet.poll();
				if (closedSet.contains(explore.location)) {
					continue;
				}
				closedSet.add(explore.location);
				evaluations++;	
				MapLocation origin = explore.location;
				Direction opposite = explore.source.opposite();
				for (Direction dir: directions) {
					MapLocation nextLocation = origin.add(dir);
					if (dir != opposite && !closedSet.contains(nextLocation) && !obstacles.containsKey(nextLocation) 
							&& bounds.isInBounds(nextLocation)) {
						double rubble = 0; 
						double pathLength = explore.pathLength+1;
						if (robot.canSense(nextLocation)) {
							rubble = robot.senseRubble(nextLocation);
							if (rubble > GameConstants.RUBBLE_SLOW_THRESH) {
								obstacles.put(nextLocation, rubble);
							}
						}
						else {
							Double value = obstacles.get(nextLocation);
							if (value != null) {
								rubble = value;
							} else {
								pathLength += unexploredTilePenalty;
							}
						}
						Node toExplore = new Node(nextLocation, dir, explore, pathLength, rubble, rubbleClearTurns);
						openSet.add(toExplore);
						if (toExplore.location.distanceSquaredTo(destination) <= 1) {
							Node node = toExplore;
							while (node.location != source) {
								route.push(node);
								node = node.origin;
							}
							isDestinationFound = true;
							oldRoute.clear();
							//robot.setIndicatorString(1, "ROUTE FOUND "+robot.getRoundNum());
							return;
						}
					}
				}
			}
		}
	}
	
	/** Returns the best route using the information gathered so far **/
	public void estimateRoute() {
		Node node = openSet.peek();
		String str = "";
		while (!openSet.isEmpty()) {
			str += openSet.poll().location;
		}
		str += "end";
		//robot.setIndicatorString(2, str);
		String str2 = "";
		for (MapLocation n : closedSet) {
			str2 += n;
		}
		str2 += "end";
		//robot.setIndicatorString(1, str2);
		while (node != null && node.location != source) {
			route.push(node);
			node = node.origin;
		}
		isDestinationFound = true;
		oldRoute.clear();
		return;
	}
	
	/** Returns the location the robot is trying to path to **/
	public MapLocation destination() {
		return destination;
	}
	
	private boolean moveRandomly() throws GameActionException {
		Direction dir = Constants.DIRECTIONS[rand.nextInt(Constants.DIRECTIONS.length)];
		if (robot.canMove(dir)) {
			robot.move(dir);
			return true;
		}
		return false;
	}

	private class Node {
		public MapLocation location;
		public Direction source;
		public Node origin;
		public double pathLength; 
		public double rubble;
		public int rubbleClearTurns;

		Node(MapLocation location, Direction source, Node origin, double pathLength, double rubble, List<Double> rubbleTurns) {
			this.location = location;
			this.source = source;
			this.origin = origin;
			this.pathLength = pathLength;
			this.rubble = rubble;
			boolean rubbleSet = false;
			for(int i = 0; i < rubbleTurns.size(); i++) {
				if (rubble < rubbleTurns.get(i)) {
					this.rubbleClearTurns = i;
					rubbleSet = true;
					break;
				}
			}
			if (!rubbleSet) {
				double highestRubble = rubbleTurns.get(rubbleTurns.size()-1);
				this.rubbleClearTurns = (int)(rubbleTurns.size() + (rubble - highestRubble) / 
						(GameConstants.RUBBLE_CLEAR_PERCENTAGE*highestRubble + GameConstants.RUBBLE_CLEAR_FLAT_AMOUNT));
			}
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) 
				return true;
			if (!(o instanceof Node)) 
				return false;
			Node node = (Node) o;
			if (node.location.equals(location)) 
				return false;
			return true;
		}

		@Override
		public int hashCode() {
			return location.hashCode();
		}
	}

	private class NodeComparator implements Comparator<Node> {
		private MapLocation dest;
		private double rubblePenalty = 2;

		public NodeComparator(MapLocation dest) {
			this.dest = dest;
		}

		private int manhattanDistance(MapLocation loc1, MapLocation loc2) {
			return Math.abs(loc1.x - loc2.x) + Math.abs(loc1.y - loc2.y);
		}

		@Override
		public int compare(Node left, Node right) {
			double d =  (left.pathLength + manhattanDistance(left.location, dest) + rubblePenalty*left.rubbleClearTurns) - 
					 (right.pathLength + manhattanDistance(right.location, dest) + rubblePenalty*right.rubbleClearTurns);
			if (d > 0)
				return 1;
			else if (d < 0)
				return -1;
			else
				return 0;
		}
	}
	
	private void moveToward(Direction dir) throws GameActionException {
		MapLocation myLoc = robot.getLocation();
		if (robot.canMove(dir)) {
			robot.move(dir);
		} else if (dir != Direction.OMNI) {
			Direction leftDir = dir.rotateLeft();
			if (robot.canMove(leftDir)) {
				robot.move(leftDir);
			} else {
				Direction rightDir = dir.rotateRight();
				if (robot.canMove(rightDir)) {
					robot.move(rightDir);
				} else if (!robot.getType().ignoresRubble && robot.onTheMap(robot.getLocation().add(dir))){
					double minRubble = Double.MAX_VALUE;
					Direction toClear = Direction.OMNI;
					double rRubble = robot.senseRubble(myLoc.add(rightDir));
					if (rRubble > 0 && rRubble < minRubble) {
						minRubble = rRubble;
						toClear = rightDir;
					}
					double lRubble = robot.senseRubble(myLoc.add(leftDir));
					if (lRubble > 0 && lRubble < minRubble) {
						minRubble = lRubble;
						toClear = leftDir;
					}
					double nRubble = robot.senseRubble(myLoc.add(dir));
					if (nRubble > 0 && nRubble < minRubble) {
						minRubble = nRubble;
						toClear = dir;
					}
					if (toClear != Direction.OMNI) {
						// Cheap fix for TTMs not being able to clear rubble
						if (robot.getType() != RobotType.TURRET && robot.getType() != RobotType.TTM)
							robot.clearRubble(toClear);
					}
				}
			}
		}
	}
}
