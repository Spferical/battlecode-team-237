package team237;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Signal;
import battlecode.common.Team;

public class ExplorerScout extends BaseScout {
	private Map<Integer, SightedRobot> zombieDens = new HashMap<>();
	private Map<Integer, SightedRobot> friendlyArchons = new HashMap<>();
	private Map<Integer, SightedRobot> enemyArchons = new HashMap<>();

	// NOTE: locations are removed from the lists as archons are found
	private List<MapLocation> initialEnemyArchonLocations = new ArrayList<MapLocation>();
	private List<MapLocation> initialFriendlyArchonLocations = new ArrayList<MapLocation>();

	private int[][] chunks;
	private int[] chunkTarget;

	private Direction mapBoundsTarget = null;

	public ExplorerScout(RobotController rc, Signal[] messages) {
		super(rc, messages);

		state = State.DEFAULT;
		initialEnemyArchonLocations = new ArrayList<MapLocation>(Arrays.asList(rc.getInitialArchonLocations(oppTeam)));
		initialFriendlyArchonLocations = new ArrayList<MapLocation>(Arrays.asList(rc.getInitialArchonLocations(ourTeam)));
		handleInitialSignals(messages);
	}

	@Override
	public void run() {
		while (true) {
			try {
				upkeep();
				if (!rc.isArmageddonDaytime()) {
					return;
				}
				rc.setIndicatorString(2, "Bounds: W:" + bounds.mapLeft + " E:" + bounds.mapRight + " N:" + bounds.mapTop + " S:" + bounds.mapBottom);
				switch (state) {
				case DEFAULT:
					if (mapBoundsTarget == null) {
						mapBoundsTarget = getMapBoundsTarget();
					}
					MapLocation test = rc.getLocation();
					test = test.add(mapBoundsTarget, 100);
					if (!bounds.isInBounds(test)) {
						// we found our target! either through signals or
						// through detecting it
						mapBoundsTarget = getMapBoundsTarget();
					}
					if (mapBoundsTarget.equals(Direction.NONE)
							|| mapBoundsTarget.equals(Direction.OMNI)) {
						// we found all the map edges!
						// transition into exploring the map itself
						state = State.EXPLORE;
						// start exploring this turn!
						continue;
					}
					rc.setIndicatorString(1, "Searching for bound: " + mapBoundsTarget);
					if (rc.isCoreReady()) {
						moveToward(mapBoundsTarget);
						updateBounds();
						lookAround();
					}
					break;
				case EXPLORE: // exploring!
					if (rc.isCoreReady()) {
						explore();
						lookAround();
						if (shouldReturn()) {
							state = State.RETURN_TO_ARCHON;
						}
					}
					break;
				case RETURN_TO_ARCHON:
					if (!shouldReturn()) {
						state = State.DEFAULT;
					}
					else if (rc.getHealth() >= Constants.SCOUT_DANGER_HEALTH_PERCENTAGE * rc.getType().maxHealth) {
						lookAround();
						if (rc.isCoreReady()) {
							if (!moveTowardsClosestFriendlyArchon()) {
								// we don't know where to look!
								// just go back to exploring...
								state = State.DEFAULT;
							}
						}					
					}
					else {
						if(rc.isCoreReady()) {
							RobotInfo robots[] = rc.senseNearbyRobots();
							boolean runAway = false;
							for (RobotInfo rob : robots) {
								if (rob.team == Team.ZOMBIE && rob.type != RobotType.ZOMBIEDEN) {
									if (rob.type.attackRadiusSquared < rob.location.distanceSquaredTo(rc.getLocation())) {
										runAway = true;
										break;
									}
								}
							}
							if (runAway) {
								runTowardEnemy(robots);
							}
							else {
								moveTowardsClosestFriendlyArchon();
							}
						}					
					}
					break;
				}
				Clock.yield();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
	}

	/** Returns whether there were any friendly archons found to return to. */
	private boolean moveTowardsClosestFriendlyArchon() throws GameActionException{
		int closestDistance = Integer.MAX_VALUE;
		RobotInfo closestArchon = null;
		for (SightedRobot archon : friendlyArchons.values()) {
			int distance = rc.getLocation().distanceSquaredTo(archon.info.location);
			if (distance < closestDistance) {
				closestDistance = distance;
				closestArchon = archon.info;
			}
		}
		if (closestArchon != null) {
			moveToward(closestArchon.location);
			return true;
		}
		return false;
	}

	private void getNewTargetChunk() {
		MapLocation loc = rc.getLocation();
		int[] here = getChunk(loc);

		int dx = Math.max(loc.x - bounds.mapLeft, bounds.mapRight - loc.x);
		int dy = Math.max(loc.y - bounds.mapTop, bounds.mapBottom - loc.y);
		int maxDist = Math.max(dx, dy);

		for (int dist = 1; dist <= maxDist; dist++) {
			List<int[]> goodTargets = new ArrayList<>();
			// top and bottom scan
			int mapWidth = chunks.length;
			int mapHeight = chunks[0].length;
			int xMin = Math.max(here[0] - dist, 0);
			int xMax = Math.min(here[0] + dist, mapWidth - 1);
			int yMin = Math.max(here[1] - dist, 0);
			int yMax = Math.min(here[1] + dist, mapHeight - 1);
			int[] ys = {yMin, yMax};
			for (int x = xMin; x <= xMax; x++) {
				for (int y : ys) {
					if (chunks[x][y] == 0) {
						int[] chunk = {x, y};
						goodTargets.add(chunk);
					}
				}
			}
			// left and right scan
			int[] xs = {xMin, xMax};
			for (int y = yMin; y <= yMax; y++) {
				for (int x : xs) {
					if (chunks[x][y] == 0) {
						int[] chunk = {x, y};
						goodTargets.add(chunk);
					}
				}
			}
			if (!goodTargets.isEmpty()) {
				int index = rand.nextInt(goodTargets.size());
				chunkTarget = goodTargets.get(index);
				rc.setIndicatorString(1, "EXPLORING TO CHUNK " +
						chunkTarget[0] + ", " + chunkTarget[1]);
			}
		}
		// if we get here, there are no more chunks to explore!
		// so lets reset everything and explore anew!
		chunks = allocateChunks();
	}

	/** If a zombie den is found for the first time, broadcast its location **/
	private void signalZombieDen(RobotInfo zombieDen) throws GameActionException {
		if (!zombieDens.containsKey(zombieDen.ID)) {
			zombieDens.put(zombieDen.ID, new SightedRobot(zombieDen, rc.getRoundNum()));
			rc.broadcastMessageSignal(Constants.ZOMBIE_DEN_FOUND_CODE, encodePosition(zombieDen.location), getMaxSignalRange());
		}
	}

	private void runTowardEnemy(RobotInfo[] robots) throws GameActionException {
		double friendx = 0;
		double friendy = 0;
		double friendWeight = 2;
		int friends = 0;
		for (MapLocation location : initialEnemyArchonLocations) {
			friendx += location.x;
			friendy += location.y;
			friends++;
		}
		for (SightedRobot rob : enemyArchons.values()) {
			MapLocation location = rob.info.location;
			friendx += location.x;
			friendy += location.y;
			friends++;
		}
		if (friends > 0) {
			friendx /= friends;
			friendy /= friends;
		}
		double oppx = 0;
		double oppy = 0;
		double oppWeight = 2;
		int opps = 0;
		for (MapLocation location : initialFriendlyArchonLocations) {
			oppx += location.x;
			oppy += location.y;
			opps++;
		}
		for (SightedRobot archon : friendlyArchons.values()) {
			MapLocation location = archon.info.location;
			oppx += location.x;
			oppy += location.y;
			opps++;
		}
		if (opps > 0) {
			oppx /= opps;
			oppy /= opps;
		}
		double zomx = 0;
		double zomy = 0;
		double zomWeight = 1;
		int zoms = 0;
		for (RobotInfo robot : robots) {
			if (robot.team == Team.ZOMBIE) {
				zomx += robot.location.x;
				zomy += robot.location.y;
				zoms++;
			}
		}
		if (zoms > 0) {
			zomx /= zoms;
			zomy /= zoms;
		}
		MapLocation curLoc = rc.getLocation();
		double curx = curLoc.x;
		double cury = curLoc.y;
		double fx = friendx - curx;
		double fy = friendy - cury;
		double fl = Math.sqrt(fx*fx+fy*fy);
		double ox = oppx - curx;
		double oy = oppy - cury;
		double ol = Math.sqrt(ox*ox+oy*oy);
		double zx = zomx - curx;
		double zy = zomy - cury;
		double zl = Math.sqrt(zx*zx+zy*zy);
		double diffx = (fx/fl) * friendWeight - (ox/ol) * oppWeight - (zx/zl) * zomWeight;
		double diffy = (fy/fl) * friendWeight - (oy/ol) * oppWeight - (zy/zl) * zomWeight;
		Direction dir = directionTo(diffx, diffy);
		moveToward(curLoc.add(dir));
	}
	
	@Override
	protected void upkeep() throws GameActionException {
		if (chunks == null && bounds.allBoundsKnown()) {
			chunks = allocateChunks();
		}
		handleSignals();

		rc.setIndicatorString(0, ""+state);
	}
	
	void removeNearest(List<MapLocation> locs, MapLocation loc) {
		if (!locs.isEmpty()) {
			MapLocation nearest = closest(locs, loc);
			locs.remove(nearest);
		}
	}

	Direction updateBounds() throws GameActionException {
		Direction update = bounds.update(rc);
		// signal update, if any
		switch (update) {
		case SOUTH:
			rc.broadcastMessageSignal(Constants.BOTTOM_MAP_BOUND_CODE,
					bounds.mapBottom, getMaxSignalRange());
			break;
		case WEST:
			rc.broadcastMessageSignal(Constants.LEFT_MAP_BOUND_CODE,
					bounds.mapLeft, getMaxSignalRange());
			break;
		case EAST:
			rc.broadcastMessageSignal(Constants.RIGHT_MAP_BOUND_CODE,
					bounds.mapRight, getMaxSignalRange());
			break;
		case NORTH:
			rc.broadcastMessageSignal(Constants.TOP_MAP_BOUND_CODE,
					bounds.mapTop, getMaxSignalRange());
			break;
		default:
			break;
		}
		return update;
	}

	public void handleInitialSignals(Signal[] messages) {
		// get the map bounds
		int[] xBounds = messages[1].getMessage();
		int[] yBounds = messages[2].getMessage();
		bounds.mapLeft = xBounds[0];
		bounds.mapRight = xBounds[1];
		bounds.mapTop = yBounds[0];
		bounds.mapBottom = yBounds[1];
		int s = 2;
		if (bounds.allBoundsKnown()) {
			chunks = allocateChunks();
			// get the explored chunks so far
			s = 4;
			int[] message = messages[s].getMessage();
			int x = 0;
			int y = 0;
			boolean done = false;
			while (message[0] != Constants.TRANSMISSION_END) {
				for (int i = 0; i < 2 && !done; i++) {
					for (int bit = 0; bit < 32 && !done; bit++) {
						chunks[x][y] = (message[i] >>> bit) & 1;
						x += 1;
						// wrap around once we reach right edge of map
						if (x >= chunks.length) {
							x = 0;
							y += 1;
							if (y >= chunks[0].length) {
								done = true;
							}
						}
					}
				}
				s += 1;
				message = messages[s].getMessage();
			}
		}
		s += 1;
		while (s < messages.length) {
			handleSignal(messages[s]);
			s++;
		}
	}

	public void handleSignals() {
		for (Signal s : rc.emptySignalQueue()) {
			handleSignal(s);
		}
	}

	public void handleSignal(Signal s) {
		int[]  message = s.getMessage();
		if (s.getTeam() == rc.getTeam()) {
			if (message != null) {
				int messageType = message[0];
				switch(messageType) {
				case Constants.BOTTOM_MAP_BOUND_CODE:
					bounds.mapBottom = message[1];
					if (chunks == null && bounds.allBoundsKnown()) {
						chunks = allocateChunks();
					}
					break;
				case Constants.TOP_MAP_BOUND_CODE:
					bounds.mapTop = message[1];
					if (chunks == null && bounds.allBoundsKnown()) {
						chunks = allocateChunks();
					}
					break;
				case Constants.LEFT_MAP_BOUND_CODE:
					bounds.mapLeft = message[1];
					if (chunks == null && bounds.allBoundsKnown()) {
						chunks = allocateChunks();
					}
					break;
				case Constants.RIGHT_MAP_BOUND_CODE:
					bounds.mapRight = message[1];
					if (chunks == null && bounds.allBoundsKnown()) {
						chunks = allocateChunks();
					}
					break;
				default:
					if (message[0] < 0) {
						// mark chunk as explored
						MapLocation loc = decodePosition(
								s.getLocation(), message[1]);
						if (chunks != null && bounds.isInBounds(loc)) {
							int[] chunk = getChunk(loc);
							chunks[chunk[0]][chunk[1]] = 1-message[0];
						} else {
							//System.out.println("WARNING: RECEIVED CHUNK EXPLORED SIGNAL BUT DO NOT HAVE MAP BOUNDS YET!");
						}
					}
				}
			}
		}
	}

	public Direction getMapBoundsTarget() {
		// return a random direction for which we do not know the map bound yet
		ArrayList<Direction> dirs = new ArrayList<>();
		if (bounds.mapTop == Integer.MIN_VALUE) {
			dirs.add(Direction.NORTH);
		}
		if (bounds.mapBottom == Integer.MAX_VALUE) {
			dirs.add(Direction.SOUTH);
		}
		if (bounds.mapLeft == Integer.MIN_VALUE) {
			dirs.add(Direction.WEST);
		}
		if (bounds.mapRight == Integer.MAX_VALUE) {
			dirs.add(Direction.EAST);
		}
		if (dirs.size() == 0) {
			return Direction.NONE;
		} else {
			return dirs.get(rand.nextInt(dirs.size()));
		}
	}

	private void explore() throws GameActionException{
		if (chunkTarget == null) {
			chunkTarget = getChunk(rc.getLocation());
			getNewTargetChunk();
		}
		if (chunkTarget != null && canSee(chunkTarget)) {
			// we explored the chunk!
			// lets tell everyone
			int parts = getNumPartsOnChunk(chunkTarget);
			rc.broadcastMessageSignal(
					-1-parts, encodePosition(getTopLeft(chunkTarget)),
					getMaxSignalRange());
			// and figure out where to explore next
			chunks[chunkTarget[0]][chunkTarget[1]] = 1+parts;
			getNewTargetChunk();
		}
		if (rc.isCoreReady()) {
			if (chunkTarget != null) {
				MapLocation center = getCenter(chunkTarget);
				rc.setIndicatorString(2, "MOVING TO CENTER AT " + center.x + ", " + center.y);
				moveToward(center);
			}
		}
	}

	private void lookAround() throws GameActionException {

		// handle bots we see
		RobotInfo info[] = rc.senseNearbyRobots();
		for (RobotInfo i : info) {
			if (i.team == Team.ZOMBIE) {
				// tell everyone about the zombie den we just found!
				if (i.type == RobotType.ZOMBIEDEN) {
					if (state == State.EXPLORE) {
						signalZombieDen(i);
					}
				}
			} else if (i.team == rc.getTeam()) {
				// keep track of where friendly archons are
				if (i.type == RobotType.ARCHON) {
					// update any stale information we have about this archon
					if (friendlyArchons.containsKey(i.ID)) {
						SightedRobot archon = friendlyArchons.get(i.ID);
						archon.info = i;
						archon.timestamp = rc.getRoundNum();
					} else {
						friendlyArchons.put(i.ID, new SightedRobot(i, rc.getRoundNum()));
						removeNearest(initialFriendlyArchonLocations, i.location);
					}
				}
			} else if (i.team == rc.getTeam().opponent()){
				if (i.type == RobotType.ARCHON) {
					// If we've found an enemy archon
					if (state == State.EXPLORE && !enemyArchons.containsKey(i.ID) || 
							(enemyArchons.get(i.ID) != null &&
							rc.getRoundNum() - enemyArchons.get(i.ID).timestamp > Constants.FORGET_ENEMY_ARCHON_THRESHOLD))
					{
						enemyArchons.put(i.ID, new SightedRobot(i, rc.getRoundNum()));
						rc.broadcastMessageSignal(Constants.ENEMY_ARCHON_FOUND_CODE, encodePosition(i.location), getMaxSignalRange());
						removeNearest(initialEnemyArchonLocations, i.location);
					}
				}
			}
		}

		// purge lists of units that used to exist but either died or
		// moved away from their last location
		// for now, only necessary for friendly archons
		List<Integer> idsToRemove = new ArrayList<>();
		for (SightedRobot archon : friendlyArchons.values()) {
			RobotInfo ri = archon.info;
			if (rc.canSense(ri.location)) {
				RobotInfo rob = rc.senseRobotAtLocation(ri.location);
				if (rob == null || rob.ID != ri.ID) {
					idsToRemove.add(ri.ID);
				}
			}
		}
		for (int id : idsToRemove) {
			friendlyArchons.remove(id);
		}
	}

	private boolean shouldReturn() {
		double maxHealth = rc.getType().maxHealth;
		double threshold = Constants.SCOUT_DANGER_HEALTH_PERCENTAGE * maxHealth;
		return !rc.isArmageddonDaytime() ||
			(rc.getHealth() <= threshold && !friendlyArchons.isEmpty());
	}

}
