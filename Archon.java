package team237;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Signal;
import battlecode.common.Team;
import battlecode.common.ZombieCount;
import battlecode.common.ZombieSpawnSchedule;
import team237.Constants.SpecializedRobotType;

public class Archon extends Bot {

	private enum GameState {
		EARLY_GAME, MID_GAME, LATE_GAME;
	}

	public int[] squadCount = new int[5];

	private Astar pathFinder;
	private Spawner spawner;
	private int lastTurnRepaired;
	
	private GameState currentGameState = GameState.EARLY_GAME;

	// list of info about scouts that we've detected.
	// Info about scouts that haven't responded for a while
	// (Constants.SCOUT_MIA_TIME) will be removed from the list.
	
	private PriorityQueue<TimestampedMapLocation> knownEnemyArchons = new PriorityQueue<>();

	private int numArchons;
	/** Whether you are the first, second, third or fourth archon to act 
	 * 	Assumes that each archon builds a scout on the first turn**/
	private int archonOrder;
	private List<MapLocation> activeZombieDens = new ArrayList<>();
	private int numTroops;
	private int turnsSinceCombatSignal;
	
	// Last turn that the draft was announced
	private int draftAnnounced;
	
	private int foundChunks;
		
	// list of known part locations
	private HashMap<MapLocation, PartPile> partPiles = new HashMap<>();
	private HashMap<MapLocation, Integer> neutralRobots = new HashMap<>();

	private Map<Integer, RobotType> createdRobots = new HashMap<>();

	private int[][] chunks;

	public Archon(RobotController rc) {
		super(rc);
		leaderID = rc.getID();
		leaderLoc = rc.getLocation();
		squadCount[0]=1;
		// only the first archon needs to do this, just want to avoid error when new unit is built
		pathFinder = new Astar(rc, bounds);
		double parts = rc.getTeamParts();
		numArchons = rc.getInitialArchonLocations(rc.getTeam()).length-1;
		if (rc.getRoundNum() == 0) {
		if (parts == 300) 
			archonOrder = 0;
		else if (parts <= 275)
			archonOrder = 1;
		else if (parts <= 250)
			archonOrder = 2;
		else if(parts < 225)
			archonOrder = 3;
		}
		else {
			archonOrder = numArchons-1;
		}
		
		this.spawner = new Spawner();
		this.draftAnnounced = -1;
		foundChunks = 0;
		state = State.DEFAULT;
	}

	@Override
	public void run() {
		while (true) {
			try {
				upkeep();
				if (chunks == null && bounds.allBoundsKnown()) {
					chunks = allocateChunks();
				}
				if (state == State.DEFAULT){
					determinePriorities();
				}
				// indicate our state
				if (pathFinder.destination() != null)
					rc.setIndicatorString(0, "" + state
							+ " AT " + this.currentGameState.toString()
							+ " AT LOCATION: " + pathFinder.destination()
							+ " AT TURN: " + rc.getRoundNum() + " NEARPARTS:" + partPiles.size()
							+ "NEARNEUTRALS:" + neutralRobots.size());
				else
					rc.setIndicatorString(0, "" + state
							+ " AT LOCATION: " + pathFinder.destination()
							+ " AT TURN: " + rc.getRoundNum() + " NEARPARTS:" + partPiles.size()
							+ "NEARNEUTRALS:" + neutralRobots.size());

				switch (state) {
				case DEFAULT:
					rc.setIndicatorString(1, "DOING DEFAULT STUFF");
					if (rc.isCoreReady()) {
						rc.setIndicatorString(1, "DOING DEFAULT STUFF BUILDING");
						if (numArchons == 1 || (rand.nextInt(numArchons-1)%4 == 0) || rc.getRoundNum() == 0){
							if (this.currentGameState != GameState.LATE_GAME ||
									(this.currentGameState == GameState.LATE_GAME && 
									rc.getTeamParts() > Constants.LATE_GAME_UNIT_PARTS_STOCKPILE))
								spawner.buildRobots();
						}
					}
					// move away from map border
					MapLocation myLocation = rc.getLocation();
					if (rc.isCoreReady()) {
						if (!bounds.isInBounds(myLocation.add(Direction.EAST))) {
							moveToward(myLocation.subtract(Direction.EAST));
						}
						else if (!bounds.isInBounds(myLocation.add(Direction.WEST))) {
							moveToward(myLocation.subtract(Direction.WEST));
						}
						else if (!bounds.isInBounds(myLocation.add(Direction.NORTH))) {
							moveToward(myLocation.subtract(Direction.NORTH));
						}
						else if (!bounds.isInBounds(myLocation.add(Direction.SOUTH))) {
							moveToward(myLocation.subtract(Direction.SOUTH));
						}
					}
					break;
				case ATTACK_ZOMBIEDEN:
					pathFinder.calculateRoute(3);
					if (rc.isCoreReady()) {
						if(!leaveBattlefront()) {
							int distance = rc.getLocation().distanceSquaredTo(pathFinder.destination());
							if (distance >= Constants.REROUTING_DISTANCE) {
								pathFinder.estimateRoute();
								pathFinder.moveAlongRoute();
								pathFinder.initializeRoute(pathFinder.destination());
							}
							else {
								pathFinder.moveAlongRoute();
							}
						}
					}
					break;
				case ZOMBIE_NIGHT_PREPARE:
					if (!rc.isArmageddonDaytime()) {
						rc.broadcastMessageSignal(Constants.DEFAULT_CODE, 0, Constants.ARCHON_SQUAD_RADIUS_SQAURED);
						state = State.DEFAULT;
					}
					pathFinder.calculateRoute(3);
					if (rc.isCoreReady()) {
						if(!leaveBattlefront()) {
							int distance = rc.getLocation().distanceSquaredTo(pathFinder.destination());
							if (distance >= Constants.REROUTING_DISTANCE) {
								pathFinder.estimateRoute();
								pathFinder.moveAlongRoute();
								pathFinder.initializeRoute(pathFinder.destination());
							}
							else {
								pathFinder.moveAlongRoute();
							}
						}
					}
					break;
				case ATTACK_ENEMY_ARCHON:
					if (rc.isCoreReady())
					{
						RobotInfo[] nearbyFriendlies = rc.senseNearbyRobots(type.sensorRadiusSquared,
								rc.getTeam());
						
						// # of {Soldiers, Guards, Vipers}
						int[] friendlyTypeCount = {0, 0, 0};
						for (RobotInfo friendly : nearbyFriendlies)
						{
							switch (friendly.type)
							{
							case SOLDIER:
								friendlyTypeCount[0]++;
								break;
							case GUARD:
								friendlyTypeCount[1]++;
								break;
							case VIPER:
								friendlyTypeCount[2]++;
								break;
							default:
								break;
							}
						}
						
						// If we have enough units to recruit for the draft
						if (friendlyTypeCount[0] + friendlyTypeCount[1] > Constants.DRAFT_ATTACK_SOLDIER_THRESHOLD &&
								friendlyTypeCount[2] > Constants.DRAFT_ATTACK_VIPER_THRESHOLD)
						{
							// If there actually is an enemy
							if (!this.knownEnemyArchons.isEmpty())
							{
								// Get rid of old enemy archons that have probably moved
								this.cleanPriorityQueue(this.knownEnemyArchons);
								
								// Announce that the draft has begun
								rc.broadcastMessageSignal(Constants.DRAFT_CODE,
										encodePosition(this.knownEnemyArchons.peek().mapLocation),
										Constants.ARCHON_SQUAD_RADIUS_SQAURED);
							}
						}
						this.state = State.DEFAULT;
					}
				break;
				case COLLECT_PARTS:
					MapLocation destination = pathFinder.destination();
					if (destination.distanceSquaredTo(rc.getLocation()) < Constants.PART_SENSING_RADIUS) {
						double parts = rc.senseParts(destination);
						if (parts == 0) {
							// the parts are a lie?
							RobotInfo rob = rc.senseRobotAtLocation(destination);
							if (rob == null || rob.team != Team.NEUTRAL) {
								// neutral robot is a lie
								state = State.DEFAULT;
							}
						}
					}
					if (rc.getLocation().equals(pathFinder.destination())) {
						partPiles.remove(pathFinder.destination());
						neutralRobots.remove(pathFinder.destination());
						state = State.DEFAULT;
					}
					if (pathFinder.destination().isAdjacentTo(rc.getLocation())) {
						RobotInfo robot = rc.senseRobotAtLocation(pathFinder.destination());
						if (robot != null && robot.team == Team.NEUTRAL) {
							if (rc.isCoreReady()) {
								rc.activate(pathFinder.destination());
								if (robot.type != RobotType.SCOUT) {
									createdRobots.put(robot.ID, robot.type);
									rc.broadcastMessageSignal(Constants.ASSIGN_SOLDIER_TO_SQUAD_CODE, 0, 2);
									neutralRobots.remove(pathFinder.destination());
									state = State.DEFAULT;
								} else {
									// just make them be explorer scouts for now
									RobotInfo info = rc.senseRobotAtLocation(pathFinder.destination());
									spawner.sendAssignmentTransmissions(
											SpecializedRobotType.ExplorerScout,
											info.ID);
								}
							}
						}
					}
					pathFinder.calculateRoute(3);
					if (rc.isCoreReady()) {
						int distance = rc.getLocation().distanceSquaredTo(pathFinder.destination());
						if (distance >= Constants.REROUTING_DISTANCE) {
							pathFinder.estimateRoute();
							pathFinder.moveAlongRoute();
							pathFinder.initializeRoute(pathFinder.destination());
						}
						else {
							pathFinder.moveAlongRoute();
						}
					}
					break;
				case COMBAT:
					if (rc.isCoreReady()) {
						if (getDistanceSquaredToNearestEnemy() <= RobotType.ARCHON.attackRadiusSquared) {
							leaveBattlefront();
						}
						// move towards any easy nearby neutrals if we can
						if (rc.isCoreReady()) {
							RobotInfo[] neutralbots = rc.senseNearbyRobots(Constants.PART_SENSING_RADIUS, Team.NEUTRAL);
							if (neutralbots.length != 0) {
								for (RobotInfo neutral : neutralbots) {
									Direction dir = rc.getLocation().directionTo(neutral.location);
									if (rc.canMove(dir) && rc.isCoreReady()) {
										rc.move(dir);
									}
								}
							}
						}
						// build robots
						if (rc.isCoreReady() && (numArchons == 1 || (rand.nextInt(numArchons-1)%4 == 0) || rc.getRoundNum() == 0)) {
							if (this.currentGameState != GameState.LATE_GAME ||
									(this.currentGameState == GameState.LATE_GAME && 
									rc.getTeamParts() > Constants.LATE_GAME_UNIT_PARTS_STOCKPILE))
							{
								spawner.buildRobots();
							}

						}
						// else, move towards any nearby parts
						if (rc.isCoreReady()) {
							MapLocation[] parts = rc.sensePartLocations(Constants.PART_SENSING_RADIUS);
							if (parts.length != 0) {
								for (MapLocation part : parts) {
									Direction dir = rc.getLocation().directionTo(part);
									if (rc.senseRubble(part) < 50 && rc.canMove(dir) && rc.isCoreReady()) {
										rc.move(dir);
									}
								}
							}
						}
						// else, finally move towards our destination
						if (rc.isCoreReady()) {
							moveToward(pathFinder.destination());
						}
					}
					if (turnsSinceCombatSignal >= Constants.NUM_TURNS_END_COMBAT) {
						if (rc.senseHostileRobots(rc.getLocation(), rc.getType().sensorRadiusSquared).length == 0) {
							rc.broadcastMessageSignal(Constants.DEFAULT_CODE, 0, Constants.ARCHON_SQUAD_RADIUS_SQAURED);
							state = State.DEFAULT;
						}
					}
					if (rc.getRoundNum() % 10 == 0 && state != State.ZOMBIE_NIGHT_PREPARE) {	
						rc.broadcastMessageSignal(Constants.ENTER_COMBAT_CODE, encodePosition(pathFinder.destination()), 10);
					}

					if (shouldFlee()) {
						state = State.FLEE;
					}

					break;
				case FLEE:
					// RUN
					if (rc.isCoreReady()) {
						RobotInfo[] hostiles = rc.senseHostileRobots(rc.getLocation(), -1);
						RobotInfo[] friendlies = rc.senseNearbyRobots(-1, ourTeam);
						Direction bestDirection = null;
						int bestWeight = Integer.MIN_VALUE;
						for (Direction dir : Constants.DIRECTIONS) {
							// check each tile and find out how bad an idea it is to move there
							// and move to the tile that is the least bad one
							// Higher weight is better
							int weight = 0;
							MapLocation loc = rc.getLocation().add(dir);
							if (!bounds.isInBounds(loc) || rc.senseRobotAtLocation(loc) != null) {
								continue;
							}
							// more rubble is worse
							weight -= rc.senseRubble(loc);
							// closer to center is better
							// add the distance from the nearest left/right and top/bottom edge
							weight += Math.min(loc.x - bounds.mapLeft, bounds.mapRight - loc.x);
							weight += Math.min(loc.y - bounds.mapTop, bounds.mapBottom - loc.y);
							for (RobotInfo enemy : hostiles) {
								int distance = enemy.location.distanceSquaredTo(loc);
								if (distance <= enemy.type.attackRadiusSquared) {
									// in range of enemy is worse
									weight -= enemy.type.attackPower * 100;
								}
								// more distance from enemies is better
								weight += Math.min(distance, 20) * enemy.type.attackPower;
							}
							if (weight > bestWeight) {
								bestWeight = weight;
								bestDirection = dir;
							}
						}
						if (bestDirection != null) {
							moveToward(bestDirection);
						} else {
							state = State.DEFAULT;
						}
						if (!shouldFlee()) {
							state = State.DEFAULT;
						}
					}
					break;
				case EXPLORE_CHUNK:
					pathFinder.calculateRoute(3);
					if (rc.isCoreReady()) {
						if(!leaveBattlefront()) {
							int distance = rc.getLocation().distanceSquaredTo(pathFinder.destination());
							if (distance >= Constants.REROUTING_DISTANCE) {
								pathFinder.estimateRoute();
								pathFinder.moveAlongRoute();
								pathFinder.initializeRoute(pathFinder.destination());
							}
							else {
								pathFinder.moveAlongRoute();
							}
						}
					}
					if (pathFinder.destination().distanceSquaredTo(rc.getLocation()) <= Constants.PART_SENSING_RADIUS) {
						state = State.DEFAULT;
					}
					break;
				}
				Clock.yield();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
	}

	public int getDistanceSquaredToNearestEnemy() {
		RobotInfo [] enemies = rc.senseHostileRobots(rc.getLocation(), -1);
		int minDistance = Integer.MAX_VALUE; 
		for (RobotInfo enemy : enemies) {
			int distance = rc.getLocation().distanceSquaredTo(enemy.location);
			if (distance < minDistance) {
				minDistance = distance;
			}
		}
		return minDistance;
	}


	private void handleSignals() throws GameActionException {
		for (Signal s : rc.emptySignalQueue()) {
			int[]  message = s.getMessage();
			if (s.getTeam() == rc.getTeam()) {
				if (message != null) {
					int messageType = message[0];
					if (messageType == Constants.ZOMBIE_DEN_FOUND_CODE) {
						MapLocation zombieDenLocation = decodePosition(s.getLocation(), s.getMessage()[1]);
						if(!activeZombieDens.contains(zombieDenLocation)) {
							activeZombieDens.add(zombieDenLocation);
							//rc.setIndicatorString(2, "Zombie dens found: " + activeZombieDens.size());
						}
					}
					else if (messageType == Constants.ZOMBIE_DEN_DESTROYED_CODE) {
						MapLocation zombieDen = decodePosition(s.getLocation(), message[1]);
						activeZombieDens.remove(zombieDen);
						MapLocation dest = pathFinder.destination();
						if (dest != null && dest.equals(zombieDen)) {
							rc.broadcastMessageSignal(Constants.DEFAULT_CODE, 0, Constants.ARCHON_SQUAD_RADIUS_SQAURED);
							state = State.DEFAULT;
						}
					}
					else if (messageType == Constants.ENEMY_ARCHON_FOUND_CODE) {
						MapLocation enemyArchonLocation = decodePosition(s.getLocation(), message[1]);
						this.knownEnemyArchons.add(new TimestampedMapLocation(enemyArchonLocation, rc));
					}
					else if (messageType == Constants.PARTS_HAS_BEEN_COLLECTED_CODE || 
							messageType == Constants.COLLECT_PARTS_CODE) {
						MapLocation loc = decodePosition(s.getLocation(), message[1]);
						partPiles.remove(loc);
						neutralRobots.remove(loc);
						MapLocation dest = pathFinder.destination();
						if (dest != null && dest.equals(loc)) {
							rc.broadcastMessageSignal(Constants.DEFAULT_CODE, 0, Constants.ARCHON_SQUAD_RADIUS_SQAURED);
							state = State.DEFAULT;
						}
					} else if (messageType == Constants.BOTTOM_MAP_BOUND_CODE) {
						bounds.mapBottom = message[1];
						if (chunks == null && bounds.allBoundsKnown()) {
							chunks = allocateChunks();
						}
					} else if (messageType == Constants.TOP_MAP_BOUND_CODE) {
						bounds.mapTop = message[1];
						if (chunks == null && bounds.allBoundsKnown()) {
							chunks = allocateChunks();
						}
					} else if (messageType == Constants.LEFT_MAP_BOUND_CODE) {
						bounds.mapLeft = message[1];
						if (chunks == null && bounds.allBoundsKnown()) {
							chunks = allocateChunks();
						}
					} else if (messageType == Constants.RIGHT_MAP_BOUND_CODE) {
						bounds.mapRight = message[1];
						if (chunks == null && bounds.allBoundsKnown()) {
							chunks = allocateChunks();
						}
					} else if (messageType <= 0) {
						// mark chunk as explored
						MapLocation loc = decodePosition(
								s.getLocation(), message[1]);
						if (chunks != null && bounds.isInBounds(loc)) {
							int[] chunk = getChunk(loc);
							chunks[chunk[0]][chunk[1]] = 1-message[0];
							foundChunks++;
						} else {
							//System.out.println("WARNING: RECEIVED CHUNK EXPLORED SIGNAL BUT DO NOT HAVE MAP BOUNDS YET!");
						}
					}
				}
				else
				{
					// If any other robot on our team sends us a basic signal
					if (createdRobots.containsKey(s.getID())) {
						if (state != State.COMBAT) {
							//rc.setIndicatorString(0, "Soldier " + s.getID() + " reports enemy");
							rc.broadcastMessageSignal(Constants.ENTER_COMBAT_CODE, encodePosition(s.getLocation()),
									Constants.ARCHON_SQUAD_RADIUS_SQAURED);
							state = State.COMBAT;
							pathFinder.initializeRoute(s.getLocation());
						}
						else {
							pathFinder.initializeRoute(s.getLocation());
						}
						turnsSinceCombatSignal = 0;
					}
				}
			}
		}
	}

	private boolean leaveBattlefront() throws GameActionException {
		RobotInfo robots[] = rc.senseNearbyRobots();
		double friendx = 0;
		double friendy = 0;
		double friendWeight = 0.8;
		int friends = 0;
		for (RobotInfo robot : robots) {
			if (createdRobots.containsKey(robot.ID)) {
				friendx += robot.location.x;
				friendy += robot.location.y;
				friends++;
			}
		}
		if (friends > 0) {
			friendx /= friends;
			friendy /= friends;
		}
		double oppx = 0;
		double oppy = 0;
		double oppWeight = 3;
		int opps = 0;
		for (RobotInfo robot : robots) {
			if (robot.team == oppTeam || robot.team == Team.ZOMBIE) {
				oppx += robot.location.x;
				oppy += robot.location.y;
				opps++;
			}
		}
		if (opps > 0) {
			oppx /= opps;
			oppy /= opps;
		}
		MapLocation curLoc = rc.getLocation();
		double curx = curLoc.x;
		double cury = curLoc.y;
		double fx = friendx - curx;
		double fy = friendy - cury;
		double fl = Math.sqrt(fx*fx+fy*fy);
		double ox = oppx - curx;
		double oy = oppy - cury;
		double ol = Math.sqrt(ox*ox+oy*oy);
		double diffx = (fx/fl) * friendWeight - (ox/ol) * oppWeight;
		double diffy = (fy/fl) * friendWeight - (oy/ol) * oppWeight;
		Direction dir = directionTo(diffx, diffy);
		RobotInfo nearest = closest(rc.senseHostileRobots(curLoc, rc.getType().sensorRadiusSquared));
		if (opps > 0 && nearest != null && curLoc.distanceSquaredTo(nearest.location) < 30) {
			moveToward(curLoc.add(dir));
			return true;
		}
		return false;
	}

	protected void updateKnownEnemyLocations() throws GameActionException{
		List<MapLocation> goneZombieDens = new ArrayList<>();
		for (MapLocation loc : activeZombieDens) {
			if (rc.canSense(loc)) {
				RobotInfo rob = rc.senseRobotAtLocation(loc);
				if (rob == null || rob.type != RobotType.ZOMBIEDEN) {
					// zombie den is gone!
					rc.broadcastMessageSignal(Constants.ZOMBIE_DEN_DESTROYED_CODE,
							encodePosition(loc), getMaxSignalRange());
					goneZombieDens.add(loc);
				}

			}
		}
		for (MapLocation loc : goneZombieDens) {
			activeZombieDens.remove(loc);
		}
		if (state == State.ATTACK_ZOMBIEDEN && !activeZombieDens.contains(pathFinder.destination())) {
			state = State.DEFAULT;
		}
	}


	@Override
	protected void upkeep() throws GameActionException {
//		if (pathFinder.destination() != null)
//			rc.setIndicatorString(0, ""+state+" AT LOCATION: " + pathFinder.destination() +" AT TURN: "+rc.getRoundNum());
//		else
//			rc.setIndicatorString(0, ""+state);
//		rc.setIndicatorString(0, ""+state + " "+pathFinder.isDestinationFound + "NumTroops: " +numTroops + " "+partPiles.size());
		turnsSinceCombatSignal++;
		if (lastTurnRepaired != rc.getRoundNum()) {
			healAdjacentFriendlies();
			lastTurnRepaired = rc.getRoundNum();
		}
		if (!(state == State.COMBAT || state == State.FLEE)) {
			// enter combat state if we see any enemies
			RobotInfo[] hostiles = rc.senseHostileRobots(rc.getLocation(),rc.getType().sensorRadiusSquared);
			if (hostiles.length != 0) {
				for (RobotInfo hostile : hostiles) {
					if (hostile.type != RobotType.SCOUT) {
						rc.broadcastMessageSignal(Constants.ENTER_COMBAT_CODE, encodePosition(hostile.location),
								Constants.ARCHON_SQUAD_RADIUS_SQAURED);
						state = State.COMBAT;
						pathFinder.initializeRoute(hostile.location);
						break;
					}
				}
			}
		}
		handleSignals();
		if (chunks != null) {
			updateKnownChunkParts();
		}
		this.currentGameState = this.getGameState();
		updateKnownEnemyLocations();

		if (rc.isCoreReady()) {
			RobotInfo[] neutrals = rc.senseNearbyRobots(2, Team.NEUTRAL);
			if (neutrals.length != 0) {
				rc.activate(neutrals[0].location);
				RobotInfo robot = rc.senseRobotAtLocation(neutrals[0].location);
				if (robot.type != RobotType.SCOUT) {
					createdRobots.put(robot.ID, robot.type);
					rc.broadcastMessageSignal(Constants.ASSIGN_SOLDIER_TO_SQUAD_CODE, 0, 2);
					neutralRobots.remove(neutrals[0].location);
					state = State.DEFAULT;
				} else {
					// just make them be explorer scouts for now
					spawner.sendAssignmentTransmissions(
							SpecializedRobotType.ExplorerScout,
							robot.ID);
				}
			}
		}
		
		MapLocation[] partLocs = rc.sensePartLocations(-1);
		for (MapLocation loc : partLocs) {
			if (!partPiles.containsKey(loc)) {
				double parts = rc.senseParts(loc);
				double rubble = rc.senseRubble(loc);
				partPiles.put(loc, new PartPile(parts, rubble, loc));
			}
		}
		RobotInfo[] neutrals = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, Team.NEUTRAL);
		for (RobotInfo robot : neutrals) {
			if (!neutralRobots.containsKey(robot.location)) {
				int value = Constants.NEUTRAL_UNIT_VALUE_WEIGHT * (robot.type.buildTurns + robot.type.partCost) - (int) rc.senseRubble(robot.location);
				if (robot.type.equals(RobotType.ARCHON)) {
					value = Constants.NEUTRAL_ARCHON_VALUE;
					state = State.DEFAULT;
				} else if (robot.type.equals(RobotType.TURRET)) {
					value = Constants.NEUTRAL_TURRET_VALUE;
				}
				neutralRobots.put(robot.location, value);
			}
		}
		
		if((rc.getRoundNum() % (300 + 900) >= (300 - 40)) && state != State.ZOMBIE_NIGHT_PREPARE && rc.isArmageddonDaytime()) {
			state = State.ZOMBIE_NIGHT_PREPARE;
			MapLocation returnTo = rc.getInitialArchonLocations(rc.getTeam())[0];
			pathFinder.initializeRoute(returnTo);
			rc.broadcastMessageSignal(Constants.RETREAT_CODE, encodePosition(returnTo),
					Constants.ARCHON_SQUAD_RADIUS_SQAURED);
		}
	}

	private void updateKnownChunkParts() throws GameActionException {
		int[] chunk = getChunk(rc.getLocation());
		if (canSee(chunk)) {
			chunks[chunk[0]][chunk[1]] = getNumPartsOnChunk(chunk);
		}
	}

	private RobotInfo lowestHealthDamaged(RobotInfo[] robots) {
		double minHealth = Double.MAX_VALUE;
		int target = 0;
		for (int i=0; i < robots.length; ++i) {
			if (robots[i].health < minHealth
					&& robots[i].health < robots[i].maxHealth) {
				minHealth = robots[i].health;
				target = i;
			}
		}
		return robots[target];
	}

	private void healAdjacentFriendlies() throws GameActionException {
		// heal the adjacent ally with the lowest hp
		// this is a FREE once-per-turn action for archons!
		RobotInfo[] friendlies = rc.senseNearbyRobots(type.attackRadiusSquared, rc.getTeam());
		if (friendlies.length > 0) {
			RobotInfo lowestHPAlly = lowestHealthDamaged(friendlies);
			double lowestHealth = lowestHPAlly.health;
			RobotType lowestType = lowestHPAlly.type;
			if (lowestType != RobotType.ARCHON && lowestHealth < lowestType.maxHealth) {
				rc.repair(lowestHPAlly.location);	
			}
		}
	}

	private void moveSquadToLocation(MapLocation location, int code) throws GameActionException {
		if (code == -1)
			rc.broadcastMessageSignal(Constants.FOLLOW_LEADER_CODE, encodePosition(location),
					Constants.ARCHON_SQUAD_RADIUS_SQAURED);
		else
			rc.broadcastMessageSignal(code, encodePosition(location),
					Constants.ARCHON_SQUAD_RADIUS_SQAURED);

		pathFinder.initializeRoute(location);
	}

	/** Figure out what the next priority should be **/
	private void determinePriorities() throws GameActionException {
		if (!rc.isArmageddonDaytime()) {
			return;
		}
		boolean isZombieDen = false;
		boolean isPartPile = false;
		MapLocation closestTarget = null;
		double biggestPriority = Integer.MIN_VALUE;
		if (!activeZombieDens.isEmpty() && partPiles.isEmpty()) {
			MapLocation toAttack = closest(activeZombieDens);
			double distance = Math.sqrt(toAttack.distanceSquaredTo(rc.getLocation()));
			double priority = Constants.ZOMBIE_DEN_PRIORITY - distance;
			if (priority > biggestPriority) {
				isZombieDen = true;
				isPartPile = false;
				closestTarget = toAttack;
				biggestPriority = priority;
			}
		}
		if (!partPiles.isEmpty()) {
			MapLocation toAttack = closest(partPiles.keySet());
			PartPile p = partPiles.get(toAttack);
			double distance = Math.sqrt(toAttack.distanceSquaredTo(rc.getLocation()));
			double priority = Constants.PART_VALUE * p.numParts - distance - p.numRubble;
			priority -= rc.getTeamParts();
			if (priority > biggestPriority) {
				closestTarget = toAttack;
				biggestPriority = priority;
				isZombieDen = false;
				isPartPile = true;
			}
		}
		if (!neutralRobots.isEmpty()) {
			MapLocation toAttack = closest(neutralRobots.keySet());
			double distance = Math.sqrt(toAttack.distanceSquaredTo(rc.getLocation()));
			double priority = neutralRobots.get(toAttack) - distance;
			if (priority > biggestPriority) {
				closestTarget = toAttack;
				biggestPriority = priority;
				isZombieDen = false;
				isPartPile = false;
			}
		}

		if (chunks != null) {
			int[] currentChunk = getChunk(rc.getLocation());
			int partsOnCurrentChunk = chunks[currentChunk[0]][currentChunk[1]] - 1;
			for (int x = 0; x < chunks.length; x++) {
				for (int y = 0; y < chunks[0].length; y++) {
					int parts = chunks[x][y] - 1;
					if (parts > 2 + partsOnCurrentChunk && !(x == currentChunk[0] && y == currentChunk[1])) {
						int[] chunk = new int[2];
						chunk[0] = x;
						chunk[1] = y;
						MapLocation center = getCenter(chunk);
						double distance = Math.sqrt(rc.getLocation().distanceSquaredTo(center));
						double priority = Constants.PART_VALUE * parts - distance;
						if (priority > biggestPriority) {
							//System.out.println("" + parts + " " + partsOnCurrentChunk + " " + x + " " + y + " " + currentChunk[0] + " " + currentChunk[1]);
							closestTarget = center;
							biggestPriority = priority;
						}
					}
				}
			}
		}

		if (isZombieDen == true || rc.getTeamParts() > Constants.LATE_GAME_PARTS_STOCKPILE) {
			// If it's late game
			if (this.currentGameState == GameState.LATE_GAME)
			{
				// And it's been a while since the last draft
				if (rc.getRoundNum() - this.draftAnnounced > Constants.DRAFT_TURNS_THRESHOLD)
				{
					state = State.ATTACK_ENEMY_ARCHON;
					this.draftAnnounced = rc.getRoundNum();
					return;
				}
			}
		}
		
		if (closestTarget == null)
			return;
		
		if (rc.getTeamParts() > Constants.PART_STOCKPILE && (biggestPriority < Constants.OVERRIDING_PRIORITY_VALUE)) {
			//Keep building robots to not waste parts
			return;
		}
		
		if (activeZombieDens.contains(closestTarget)) {
			pathFinder.initializeRoute(closestTarget);
			state = State.ATTACK_ZOMBIEDEN;
			rc.broadcastMessageSignal(Constants.ATTACK_ZOMBIE_DEN_CODE, encodePosition(closestTarget),
					Constants.ARCHON_SQUAD_RADIUS_SQAURED);
		}
		else if (partPiles.keySet().contains(closestTarget)) {
			pathFinder.initializeRoute(closestTarget);
			state = State.COLLECT_PARTS;
			rc.broadcastMessageSignal(Constants.COLLECT_PARTS_CODE, encodePosition(closestTarget),
					Constants.ARCHON_SQUAD_RADIUS_SQAURED);
		} else if (neutralRobots.keySet().contains(closestTarget)) {
			pathFinder.initializeRoute(closestTarget);
			state = State.COLLECT_PARTS;
			rc.broadcastMessageSignal(Constants.COLLECT_PARTS_CODE, encodePosition(closestTarget),
					Constants.ARCHON_SQUAD_RADIUS_SQAURED);
		} else {
			pathFinder.initializeRoute(closestTarget);
			state = State.EXPLORE_CHUNK;
			rc.broadcastMessageSignal(Constants.COLLECT_PARTS_CODE, encodePosition(closestTarget),
					Constants.ARCHON_SQUAD_RADIUS_SQAURED);			
		}
	}
	
	/** 
	 * @return current state of the game (early game, mid game, late game)
	 */
	public GameState getGameState() {
		if (this.currentGameState == GameState.LATE_GAME)
			return GameState.LATE_GAME;
		boolean mapIsFullyExplored = this.mapIsFullyExplored();
		if (rc.getRoundNum() < Constants.EARLY_GAME_CUTOFF && !mapIsFullyExplored)
			return GameState.EARLY_GAME;
		else if (bounds.getAllKnownCorners().size() >= 2 
				&& rc.getRoundNum() < Constants.LATE_GAME_CUTOFF) {
			return GameState.MID_GAME;
		} else {
			List<MapLocation> enemyArchons = new ArrayList<MapLocation>();
			Iterator<TimestampedMapLocation> it = this.knownEnemyArchons.iterator();
			
			for (MapLocation mapLoc : rc.getInitialArchonLocations(oppTeam))
				enemyArchons.add(mapLoc);
			
			while (it.hasNext())
			{
				TimestampedMapLocation enemy = it.next();
				if (!enemyArchons.contains(enemy.mapLocation))
					enemyArchons.add(enemy.mapLocation);
			}
			
			MapLocation closestEnemyArchon = this.closest(enemyArchons);
			if (rc.getRoundNum() > Constants.LATE_GAME_CUTOFF)
			{
				if (closestEnemyArchon == null || this.activeZombieDens.isEmpty())
					return GameState.LATE_GAME;
				MapLocation closestZombieDenToUs = this.closest(this.activeZombieDens);
				MapLocation closestZombieDenToThem = this.closest(this.activeZombieDens, 
					closestEnemyArchon);
				if (rc.getRoundNum() < Constants.VERY_LATE_GAME_CUTOFF || 
						closestZombieDenToUs.distanceSquaredTo(rc.getLocation()) < 
						closestZombieDenToThem.distanceSquaredTo(rc.getLocation()))
					return GameState.LATE_GAME;
				else
					return GameState.MID_GAME;
			}
			else
				return GameState.EARLY_GAME;
		}
	}

	public boolean mapIsFullyExplored() {
		if (chunks == null) {
			return false;
		}
		for (int x = 0; x < chunks.length; x++) {
			for (int y = 0; y < chunks[0].length; y++) {
				if (chunks[x][y] == 0) {
					return false;
				}
			}
		}
		return true;
	}
	
	
	/**
	 * Handles spawning units based on Archon state... more to come soon
	 */
	private class Spawner {
		
		/** Try to build a robot.  */
		public void buildRobots() throws GameActionException {
			SpecializedRobotType robotToBuild = determineRobotToBuild();
			rc.setIndicatorString(2,robotToBuild+"");
			int id = this.buildRobot(SpecializedRobotType.getRobotType(robotToBuild));
			if (id > 0)
				this.sendAssignmentTransmissions(robotToBuild, id);
		}
	
		/** Attempts to build the specified robot. The robot is built in a random direction.
		 *	boolean is used to increment squad count
		 *  Returns ID of the robot if it was built, otherwise -1 **/
		private int buildRobot(RobotType typeToBuild) throws GameActionException {
			if (typeToBuild.partCost > rc.getTeamParts()) {
				return -1;
			}
			rc.setIndicatorString(2,rc.getTeamParts()+" NOT PART COST");
			rc.setIndicatorString(2,archonOrder+" "+numArchons+" NOT ORDER");
			if (rc.getRoundNum() > 0) {
				if ((double)(rand.nextInt(256))/256 > (double)(1)/(numArchons - archonOrder)) {
					return -1;
				}
			}
			rc.setIndicatorString(2,archonOrder+" TRYING TO BUILD ");	
	
			Direction dirToBuild = Direction.values()[rand.nextInt(8)];
			for (int i = 0; i < 8; ++i) {
				if (rc.canBuild(dirToBuild, typeToBuild)) {
					rc.build(dirToBuild, typeToBuild);
					RobotInfo createdBot = rc.senseRobotAtLocation(rc.getLocation().add(dirToBuild));
					createdRobots.put(createdBot.ID, createdBot.type);
					//rc.broadcastMessageSignal(Constants.ASSIGN_SOLDIER_TO_SQUAD_CODE, 0, 2);
					return createdBot.ID;
				} else {
					dirToBuild = dirToBuild.rotateLeft();
				}
			}
			return -1;
		}
		
		private SpecializedRobotType determineRobotToBuild() {
			SpecializedRobotType selectedRobot = null;
			boolean zombiesAreDangerous = this.areManyZombiesGoingToSpawn();
			if (currentGameState == GameState.EARLY_GAME)
			{
				if (knownEnemyArchons.size() > 2 && activeZombieDens.size() > 1 && bounds.getAllKnownCorners().size() >= 2)
				{
					if (zombiesAreDangerous)
						selectedRobot = pickFromDistribution(Constants.EARLY_GAME_MANY_ZOMBIES_SPAWN);
					else
						selectedRobot = pickFromDistribution(Constants.EARLY_GAME_DEFAULT_SPAWN);
				}
				else
				{
					if (zombiesAreDangerous)
						selectedRobot = pickFromDistribution(Constants.STARTING_GAME_MANY_ZOMBIES_SPAWN);
					else
						selectedRobot = pickFromDistribution(Constants.STARTING_GAME_DEFAULT_SPAWN);
				}
			} else if (currentGameState == GameState.MID_GAME) {
				// If we need to explore more, spawn more scouts
				if (chunks != null && Archon.this.foundChunks < (chunks.length * chunks[0].length) * 
						Constants.PERCENTAGE_MAP_EXPLORED_TO_NOT_EXPLORE_MORE)
				{
					selectedRobot = pickFromDistribution(Constants.MID_GAME_EXPLORE_MORE_SPAWN);
				}
				else
				{
					if (zombiesAreDangerous)
						selectedRobot = pickFromDistribution(Constants.MID_GAME_MANY_ZOMBIES_SPAWN);
					else
						selectedRobot = pickFromDistribution(Constants.MID_GAME_DEFAULT_SPAWN);
				}
			} else if (currentGameState == GameState.LATE_GAME) {
				if (zombiesAreDangerous)
					selectedRobot = pickFromDistribution(Constants.LATE_GAME_MANY_ZOMBIES_SPAWN);
				else
					selectedRobot = pickFromDistribution(Constants.LATE_GAME_DEFAULT_SPAWN);
			} else
				selectedRobot = SpecializedRobotType.ExplorerScout;
			
			if (selectedRobot == SpecializedRobotType.ZombieCowboyScout &&
					activeZombieDens.isEmpty())
				return determineRobotToBuild();
			else
				return selectedRobot;
		}
		
		private boolean areManyZombiesGoingToSpawn() {
			ZombieSpawnSchedule schedule = rc.getZombieSpawnSchedule();
			int nextRound = -1;
			for (int round : schedule.getRounds())
			{
				if (round > rc.getRoundNum())
				{
					nextRound = round;
					break;
				}
			}
			
			// If the next round is a very long time away, we don't care too much about zombies
			if (rc.getRoundNum() - nextRound > Constants.ARCHON_ZOMBIE_FORECAST)
				return false;
			
			ZombieCount[] zombies = schedule.getScheduleForRound(nextRound);
			
			//								Regular	Fast	Ranged	Big
			int zombiesCount[] = new int[] {0,		0,		0,		0};
			int score = 0;
			
			for (ZombieCount zombie : zombies)
			{
				switch (zombie.getType())
				{
				case STANDARDZOMBIE:
					zombiesCount[0]++;
					score += RobotType.STANDARDZOMBIE.attackPower(rc.getRoundNum());
					break;
				case FASTZOMBIE:
					zombiesCount[1]++;
					score += RobotType.FASTZOMBIE.attackPower(rc.getRoundNum());
					break;
				case RANGEDZOMBIE:
					zombiesCount[2]++;
					score += RobotType.RANGEDZOMBIE.attackPower(rc.getRoundNum());
					break;
				case BIGZOMBIE:
					zombiesCount[3]++;
					score += RobotType.BIGZOMBIE.attackPower(rc.getRoundNum());
					break;
				default:
					break;
				}
			}
			
			if (Archon.this.currentGameState != GameState.EARLY_GAME)
				score *= Archon.this.activeZombieDens.size();
			else
				score *= Constants.ARCHON_EARLY_GAME_ZOMBIE_CAUTIOUSNESS;
			
			if (score > Constants.ARCHON_ZOMBIE_DANGER_SCORE)
				return true;
			else
				return false;
		}
		
		private void sendAssignmentTransmissions(SpecializedRobotType type, int robotID) throws GameActionException {
			switch (type) {
			case ZombieCowboyScout:
				Iterator<TimestampedMapLocation> it = knownEnemyArchons.iterator();
				rc.broadcastMessageSignal(Constants.SCOUT_ZOMBIE_COWBOY, 0, 2);
				
				int[] enemyArchonData = new int[(knownEnemyArchons.size() > Constants.ZOMBIE_COWBOY_OBJECTIVES_NUM) ? 
						Constants.ZOMBIE_COWBOY_OBJECTIVES_NUM * 2 : activeZombieDens.size() * 2];
				int[] zombieDenData = new int[(activeZombieDens.size() > Constants.ZOMBIE_COWBOY_OBJECTIVES_NUM) ? 
						Constants.ZOMBIE_COWBOY_OBJECTIVES_NUM : activeZombieDens.size()];
				for (int i = 0; i < enemyArchonData.length; i += 2)
				{
					if (it.hasNext())
					{
						TimestampedMapLocation nextArchon = it.next();
						enemyArchonData[i] = encodePosition(nextArchon.mapLocation);
						enemyArchonData[i + 1] = nextArchon.roundNumber;
					}
				}
				for (int i = 0; i < zombieDenData.length; i++)
					zombieDenData[i] = encodePosition(activeZombieDens.get(i));
				
				// Tells the zombie cowboy of archons in order of priority
				sendTransmission(robotID, 2, enemyArchonData);
				// Tells the zombie cowboy of zombie dens in order of priority
				sendTransmission(robotID, 2, zombieDenData);
				break;
			case ExplorerScout:
				rc.broadcastMessageSignal(Constants.SCOUT_EXPLORER, 0, 2);
				// communicate the map bounds
				rc.broadcastMessageSignal(bounds.mapLeft, bounds.mapRight, 2);
				rc.broadcastMessageSignal(bounds.mapTop, bounds.mapBottom, 2);
				if (bounds.allBoundsKnown()) {
					// communicate the so-far explored chunks
					int numChunks = chunks.length * chunks[0].length;
					int numInts = numChunks / 32;
					if (numChunks % 32 != 0) {
						numInts += 1;
					}
					int[] data = new int[numInts];
					int indexInInt = 0;
					int numInt = 0;
					for (int y = 0; y < chunks[0].length; y++) {
						for (int x = 0; x < chunks.length; x++) {
							int num = data[numInt];
							int explored = (chunks[x][y] != 0)? 1 : 0;
							data[numInt] = num | (explored << indexInInt);
							indexInInt += 1;
							if (indexInInt >= 32) {
								indexInInt = 0;
								numInt += 1;
							}
						}
					}
					sendTransmission(robotID, 2, data);
				}
				break;
			case Soldier:
				rc.broadcastMessageSignal(Constants.ASSIGN_SOLDIER_TO_SQUAD_CODE, 0, 2);
				break;
			case Guard:
				rc.broadcastMessageSignal(Constants.ASSIGN_SOLDIER_TO_SQUAD_CODE, 0, 2);
				break;
			default:
				rc.broadcastMessageSignal(Constants.ASSIGN_SOLDIER_TO_SQUAD_CODE, 0, 2);
				break;
			}
		}
		
		private SpecializedRobotType pickFromDistribution(double[] distribution) {
			double accum = 0;
			int start = 0;
			double randDouble = rand.nextDouble();
			SpecializedRobotType[] robotTypes = SpecializedRobotType.values();
			for (int i = start; i < robotTypes.length; i++)	
			{
				accum += distribution[i];
				if (randDouble < accum)
					return robotTypes[i];
			}
			System.out.println("Invalid distribution!");
			return null;
		}
	}

	private boolean shouldFlee() {
		RobotInfo[] friends = rc.senseNearbyRobots(-1, ourTeam);
		RobotInfo[] enemies = rc.senseHostileRobots(rc.getLocation(), -1);
		int numFriends = 0;
		int numEnemies = 0;
		for (RobotInfo friend : friends) {
			if (friend.type.canAttack()) {
				numFriends++;
			}
		}
		for (RobotInfo enemy : enemies) {
			if (enemy.type.canAttack()) {
				numEnemies++;
			}
		}
		return (numFriends + 1) * 2 < numEnemies;
	}
}
