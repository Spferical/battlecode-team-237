package team237;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Signal;

public abstract class Bodyguard extends Bot {

	protected int leaderID;
	protected MapLocation leaderLoc;

	public Bodyguard(RobotController robotController) {
		super(robotController);
		Signal first = rc.readSignal();
		leaderID = first.getID();
		leaderLoc = first.getLocation();
		state = State.DEFAULT;
	}

	@Override
	public void run() {
		while (true) {
			try {
				upkeep();
				rc.setIndicatorString(0, "" + state);
				switch (state) {
				case ATTACK_ZOMBIEDEN:
					pathFinder.calculateRoute(3);
					if (rc.isCoreReady()) {
						int distance = rc.getLocation().distanceSquaredTo(pathFinder.destination());
						if (distance >= Constants.REROUTING_DISTANCE) {
							pathFinder.estimateRoute();
							pathFinder.moveAlongRoute();
							pathFinder.initializeRoute(pathFinder.destination());
						} else {
							pathFinder.moveAlongRoute();
						}
					}
					break;
				case COLLECT_PARTS:
					pathFinder.calculateRoute(3);
					if (rc.getLocation().distanceSquaredTo(pathFinder.destination()) <= leaderLoc
							.distanceSquaredTo(pathFinder.destination()) && rc.isCoreReady()) {
						if (rc.getLocation().distanceSquaredTo(pathFinder.destination()) <= 3) {
							Direction away = pathFinder.destination().directionTo(rc.getLocation());
							if (away == Direction.OMNI) {
								moveRandomly();
							} else {
								moveToward(rc.getLocation().add(away));
							}
						} else {
							decrowd();
							if (rc.isCoreReady()) {
								cleanNearbyRubble();
							}
						}
					}
					if (rc.isCoreReady()) {
						int distance = rc.getLocation().distanceSquaredTo(pathFinder.destination());
						if (distance >= Constants.REROUTING_DISTANCE) {
							pathFinder.estimateRoute();
							pathFinder.moveAlongRoute();
							pathFinder.initializeRoute(pathFinder.destination());
						} else {
							pathFinder.moveAlongRoute();
						}
					}
					break;
				case COMBAT:
					// suicide only if infected by vipers and has low enough
					// health
//					if (rc.isInfected()) {
//						// suicide toward closest enemy
//						suicideRun();
//					}
					// If injured, go to archon to heal
					if (rc.isCoreReady() && rc.getHealth() < rc.getType().maxHealth / 2) {
						if (this.healAtLeader())
							break;
					}
					// Attempt to attack enemy
					if (!attackEnemy()) {
						// Calculate route to combat
						pathFinder.calculateRoute(3);
						if (rc.isCoreReady()) {
							// Move to combat
							int distance = rc.getLocation().distanceSquaredTo(pathFinder.destination());
							if (distance >= Constants.REROUTING_DISTANCE) {
								pathFinder.estimateRoute();
								pathFinder.moveAlongRoute();
								pathFinder.initializeRoute(pathFinder.destination());
							} else {
								pathFinder.moveAlongRoute();
							}
						}
					}
					break;
				case ATTACK_ENEMY_ARCHON:
					// suicide only if infected by vipers and has low enough
					// health
					if (rc.isInfected()) {
						// suicide toward closest enemy
						suicideRun();
					}

					// Try to move towards or attack enemy
					if (!attackEnemy()) {
						pathFinder.calculateRoute(3);
						if (rc.isCoreReady()) {
							int distance = rc.getLocation().distanceSquaredTo(pathFinder.destination());
							if (distance >= Constants.REROUTING_DISTANCE) {
								pathFinder.estimateRoute();
								pathFinder.moveAlongRoute();
								pathFinder.initializeRoute(pathFinder.destination());
							} else {
								pathFinder.moveAlongRoute();
							}
						}
					}
					break;
				case ZOMBIE_NIGHT_PREPARE: 
					pathFinder.calculateRoute(3);
					if (rc.isCoreReady()) {
						int distance = rc.getLocation().distanceSquaredTo(pathFinder.destination());
						if (distance >= Constants.REROUTING_DISTANCE) {
							pathFinder.estimateRoute();
							pathFinder.moveAlongRoute();
							pathFinder.initializeRoute(pathFinder.destination());
						} else {
							pathFinder.moveAlongRoute();
						}
					}
					break;
				default:
					if (rc.isCoreReady()) {
						if (!rc.canSenseRobot(leaderID)) {
							moveToward(leaderLoc);
						}
					}
					if (rc.isCoreReady()) {
						attackEnemy();
					}
					if (rc.isCoreReady()) {
						// If health is high, try to decrowd. Otherwise come
						// back to archon
						if (rc.getHealth() == rc.getType().maxHealth) {
							decrowd();
							if (rc.isCoreReady()) {
								cleanNearbyRubble();
							}
						} else
							this.healAtLeader();
					}
					break;
				}

				Clock.yield();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	@Override
	protected void upkeep() throws GameActionException {
		if (state != State.ATTACK_ENEMY_ARCHON)
		{
			commonAlert();
			handleSignals();
		}
		else
		{
			for (Signal s : rc.emptySignalQueue())
				if (s.getID() == this.leaderID)
					leaderLoc = s.getLocation();
			if (pathFinder.destination().distanceSquaredTo(rc.getLocation()) < 25 &&
					rc.senseNearbyRobots(type.sensorRadiusSquared, rc.getTeam()).length == 0)
			{
				//System.out.println("Returning to archon!");
				pathFinder.initializeRoute(leaderLoc);
				this.state = State.RETURN_TO_ARCHON;
			}
		}
		if (rc.canSenseRobot(leaderID)) {
			RobotInfo leader = rc.senseRobot(leaderID);
			if (leader != null)
				leaderLoc = leader.location;
		}
		bounds.update(rc);
	}

	protected void commonAlert() throws GameActionException {
		if (detectEnemies() && (state != State.ZOMBIE_NIGHT_PREPARE)) {
			if (state != State.COMBAT) {
				rc.broadcastSignal(Constants.ARCHON_SQUAD_RADIUS_SQAURED);
			}
			else if ((rc.getID() + rc.getRoundNum()) % Constants.NUM_TURNS_SIGNAL_COMBAT == 0) {
				rc.broadcastSignal(Constants.ARCHON_SQUAD_RADIUS_SQAURED);
			}
		}
	}

	protected boolean detectEnemies() {
		if (rc.senseHostileRobots(rc.getLocation(), type.sensorRadiusSquared).length == 0)
			return false;
		else
			return true;
	}
	
	/**
	 * Attack without moving
	 * 
	 * @return Whether a useful action occurred or not
	 * @throws GameActionException
	 */
	protected boolean attackEnemySessile() throws GameActionException {
		int attackRange = type.attackRadiusSquared;
		RobotInfo[] enemiesWithinFire = rc.senseHostileRobots(rc.getLocation(), attackRange);
		if (enemiesWithinFire.length > 0)
		{
			MapLocation attackLoc = lowestHealth(enemiesWithinFire).location;
			rc.attackLocation(attackLoc);
			return true;
		}
		else
			return false;
	}

	/** Returns true if you perform a useful action **/
	protected boolean attackEnemy() throws GameActionException {
		int attackRange = type.attackRadiusSquared;
		int sightRange = type.sensorRadiusSquared;
		RobotInfo[] enemiesWithinSight = rc.senseHostileRobots(rc.getLocation(), sightRange);
		RobotInfo[] enemiesWithinFire = rc.senseHostileRobots(rc.getLocation(), attackRange);
		if (enemiesWithinSight.length > 0) {
			if (enemiesWithinFire.length > 0) {
				RobotInfo enemy = preferredEnemy(enemiesWithinFire);
				RobotInfo closest = closest(enemiesWithinFire);
				MapLocation attackLoc = enemy.location;
				if (rc.isCoreReady() && (closest.type.canAttack() && closest.type != RobotType.TURRET)) {
					if (rc.getLocation().distanceSquaredTo(closest(enemiesWithinFire).location) 
							<= Constants.RUN_AWAY_DISTANCE_SQAURED && rc.isCoreReady() 
							&& rc.getType().attackRadiusSquared > Constants.RUN_AWAY_DISTANCE_SQAURED) {
						kiteMaxRange(closest(enemiesWithinFire).location);
					}
				}
				if (rc.isCoreReady() && (enemy.type.canAttack() && closest.type != RobotType.TURRET)) {
					if (rc.getType().attackRadiusSquared > (attackLoc.distanceSquaredTo(rc.getLocation()) * 3) && rc.isCoreReady()) {
						kiteMaxRange(attackLoc);
					}
				}
				if (rc.isCoreReady() && enemy.type == RobotType.ZOMBIEDEN && rc.getLocation().distanceSquaredTo(attackLoc) > 2) {
					moveToward(enemy.location);
				}
				if (rc.isWeaponReady() && rc.isCoreReady()) {
					rc.attackLocation(attackLoc);
				}
			} else {
				if (rc.isCoreReady()) {
					RobotInfo idealTarget = lowestHealth(enemiesWithinSight);
					if (idealTarget.type != RobotType.SCOUT)
						moveToward(idealTarget.location);
				}
			}
			return true;
		}
		return false;
	}
	
	protected void kiteMaxRange(MapLocation loc) throws GameActionException {
		MapLocation myLoc = rc.getLocation();
		Direction dir = loc.directionTo(myLoc);
		MapLocation backLoc = loc.add(dir);
		if (rc.canMove(dir) && loc.distanceSquaredTo(backLoc) <= rc.getType().attackRadiusSquared) {
			rc.move(dir);
		} else {
			Direction leftDir = dir.rotateLeft();
			MapLocation leftLoc = loc.add(dir);
			if (rc.canMove(leftDir) && loc.distanceSquaredTo(leftLoc) <= rc.getType().attackRadiusSquared) {
				rc.move(leftDir);
			} else {
				Direction rightDir = dir.rotateRight();
				MapLocation rightLoc = loc.add(rightDir);
				if (rc.canMove(rightDir) && loc.distanceSquaredTo(rightLoc) <= rc.getType().attackRadiusSquared) {
					rc.move(rightDir);
				}
			}
		}
	}


	protected void handleSignals() {
		for (Signal s : rc.emptySignalQueue()) {
			if (rc.getTeam().equals(s.getTeam())) {
				int message[] = s.getMessage();
				if (message != null) {
					if (s.getRobotID() == leaderID) {
						leaderLoc = s.getLocation();
						if (message[0] == Constants.ATTACK_ZOMBIE_DEN_CODE) {
							handleAttackZombieDenMessage(s);
						}
						if (message[0] == Constants.RETREAT_CODE) {
							handleRetreatCode(s);
						}
						else if (message[0] == Constants.FOLLOW_LEADER_CODE) {
							handleFollowLeaderMessage(s);
						}
						else if (message[0] == Constants.ENTER_COMBAT_CODE) {
							handleEnterCombatMessage(s);
						}
						else if (message[0] == Constants.DRAFT_CODE) {
							handleDraftMessage(s);
						}
						else if (message[0] == Constants.ZOMBIE_DEN_DESTROYED_CODE) {
							handleZombieDenDestroyedMessage(s);
						}
						else if (message[0] == Constants.COLLECT_PARTS_CODE) {
							handleCollectPartsMessage(s);		
						}
						else if (message[0] == Constants.PARTS_HAS_BEEN_COLLECTED_CODE) {
							handlePartsHasBeenCollectedMessage(s);
							state = State.DEFAULT;
						}
						else if (message[0] == Constants.DEFAULT_CODE) {
							handleDefaultMessage(s);
						}
					}
				}
			}
		}
	}
	
	protected boolean healAtLeader() throws GameActionException {
		// returns whether or not we moved
		MapLocation leaderLoc = this.leaderLoc;
		if (rc.canSenseRobot(leaderID))
				leaderLoc = this.leaderLoc;
		int distanceToLeader =  rc.getLocation().distanceSquaredTo(leaderLoc);
		if (distanceToLeader > RobotType.ARCHON.attackRadiusSquared) {
			moveToward(leaderLoc);
			return true;
		}
		return false;
	}

	protected void handleFollowLeaderMessage(Signal s) {
		MapLocation zombieDenLocation = decodePosition(s.getLocation(), s.getMessage()[1]);
		pathFinder.initializeRoute(zombieDenLocation);
		state = State.FOLLOW_LEADER;		
	}

	protected void handleEnterCombatMessage(Signal s) {
		MapLocation combatLocation = decodePosition(s.getLocation(), s.getMessage()[1]);
		pathFinder.initializeRoute(combatLocation);
		state = State.COMBAT;
	}
	
	protected void handleDraftMessage(Signal s) {
		// If you get drafted go into combat. Vipers are automatically enlisted.
		if (this.rand.nextDouble() < Constants.DRAFT_ODDS || type == RobotType.VIPER)
		{
			MapLocation combatLocation = decodePosition(s.getLocation(), s.getMessage()[1]);
			pathFinder.initializeRoute(combatLocation);
			state = State.ATTACK_ENEMY_ARCHON;
		}
	}

	protected void handleZombieDenDestroyedMessage(Signal s) {
		state = State.DEFAULT;		
	}

	protected void handleCollectPartsMessage(Signal s) {
		MapLocation partLocation = decodePosition(s.getLocation(), s.getMessage()[1]);
		pathFinder.initializeRoute(partLocation);
		state = State.COLLECT_PARTS;		
	}

	protected void handleAttackZombieDenMessage(Signal s) {
		MapLocation zombieDenLocation = decodePosition(s.getLocation(), s.getMessage()[1]);
		pathFinder.initializeRoute(zombieDenLocation);
		state = State.ATTACK_ZOMBIEDEN;	
	}
	
	protected void handleRetreatCode(Signal s) {
		MapLocation zombieDenLocation = decodePosition(s.getLocation(), s.getMessage()[1]);
		pathFinder.initializeRoute(zombieDenLocation);
		state = State.ZOMBIE_NIGHT_PREPARE;	
	}

	protected void handleDefaultMessage(Signal s) {
		state = State.DEFAULT;
	}

	protected void handlePartsHasBeenCollectedMessage(Signal s) {
		state = State.DEFAULT;
	}
	
	protected RobotInfo preferredEnemy(RobotInfo[] enemies) {
		double minHealth = Double.MAX_VALUE;
		int target = 0;
		for (int i=0; i < enemies.length; ++i) {
			double enemyHealth = 0;
			switch (enemies[i].type)
			{
			case VIPER:
				enemyHealth = enemies[i].health / Constants.VIPER_PRIORITY;
				break;
			case BIGZOMBIE:
				enemyHealth = enemies[i].health / Constants.BIG_ZOMBIE_PRIORITY;
				break;
			default:
				enemyHealth = enemies[i].health;
			}
			if (enemyHealth < minHealth) {
				minHealth = enemyHealth;
				target = i;
			}
		}
		return enemies[target];
	}
	
	// Am I closer to the enemy, suicide squad style
	boolean closerToEnemy (RobotInfo rob) {
		int senseRadius = rob.type.sensorRadiusSquared;
		MapLocation robLoc = rob.location;
		RobotInfo[] opponentTeam = rc.senseNearbyRobots(robLoc, senseRadius, oppTeam);
		RobotInfo[] allianceTeam = rc.senseNearbyRobots(robLoc, senseRadius, ourTeam);
		if (opponentTeam.length != 0) {
			RobotInfo closestEnemyToRob = closest(opponentTeam, robLoc);
			if (allianceTeam.length != 0) {
				RobotInfo closestAllyToRob = closest(allianceTeam, robLoc);
				MapLocation enemyLoc = closestEnemyToRob.location;
				MapLocation allyLoc = closestEnemyToRob.location;
				if (allyLoc.distanceSquaredTo(robLoc) < enemyLoc.distanceSquaredTo(robLoc)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	public void suicideRun() throws GameActionException{
		if (rc.getHealth() < Constants.VIPER_INFECT_HEALTH && 
				rc.getViperInfectedTurns() > Constants.VIPER_INFECT_HEALTH / 2) {
			if (closerToEnemy(rc.senseRobotAtLocation(rc.getLocation()))) {
				return;
			}
			else {
				RobotInfo[] opponents = rc.senseNearbyRobots(rc.getLocation(), rc.getType().sensorRadiusSquared, oppTeam);
				if (opponents.length != 0 ) {
					MapLocation target = closest(opponents).location;
					if (rc.isCoreReady() && opponents != null) {
						moveToward(target);
					}
				}
			}
		}
	}

}
