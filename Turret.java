package team237;

import java.util.LinkedList;
import java.util.Queue;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Signal;

public class Turret extends Bodyguard {
	
	protected Queue<MapLocation> reportedEnemies;
	
	public Turret(RobotController rc) {
		super(rc);
		this.reportedEnemies = new LinkedList<>();
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				if (rc.isCoreReady())
				{
					handleSignals();
					int sightRange = rc.getType().sensorRadiusSquared;
					RobotInfo[] enemiesWithinSight = rc.senseHostileRobots(
							rc.getLocation(), sightRange);
					if (rc.getType() == RobotType.TURRET)
					{
						if (enemiesWithinSight.length > 0) {
							if (rc.isWeaponReady()) {
								this.attackNearbyEnemies();
							}
						} else {
							rc.pack();
						}
					}
					else
					{
						MapLocation leaderLocation = this.getLeaderLocation();
						// If there's enemies nearby and we kind of know where the leader is, unpack and start fighting
						if (enemiesWithinSight.length > 0 && leaderLocation != null && rc.isArmageddonDaytime()) {
							rc.unpack();
						}
						// Otherwise run back to the leader
						else {
							followLeader(leaderLocation);
						}
					}
				}
				reportedEnemies.clear();
				Clock.yield();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * Determines the location of the turret's leader.
	 * 
	 * @return MapLocation of the leader if it is within sight range, null otherwise.
	 * @throws GameActionException
	 */
	private MapLocation getLeaderLocation() throws GameActionException {
		RobotInfo potentialArchonAtLocation = null;
		if (this.leaderLoc.distanceSquaredTo(rc.getLocation()) < rc.getType().sensorRadiusSquared)
			potentialArchonAtLocation = rc.senseRobotAtLocation(this.leaderLoc);
		if (potentialArchonAtLocation != null && potentialArchonAtLocation.team == rc.getTeam() &&
				potentialArchonAtLocation.ID == leaderID)
		{
			assert(potentialArchonAtLocation.type == RobotType.ARCHON);
			return potentialArchonAtLocation.location;
		}
		else {
			RobotInfo[] nearbyUnitLocations = rc.senseNearbyRobots(-1, rc.getTeam());
			for (RobotInfo info : nearbyUnitLocations)
			{
				if (info.ID == leaderID)
				{
					assert(info.type == RobotType.ARCHON);
					return info.location;
				}
			}
		}
		return null;
	}

	private void followLeader(MapLocation leaderLoc) throws GameActionException {
		MapLocation leaderLocation = this.getLeaderLocation();
		// If we lost sight of the leader, go to last observed leader location
		if (leaderLocation != null)
		{
			this.pathFinder.initializeRoute(leaderLocation);
			this.leaderLoc = leaderLocation;
		}
		else
			this.pathFinder.initializeRoute(this.leaderLoc);
		
		double leaderDist = this.leaderLoc.distanceSquaredTo(rc.getLocation());
		
		// The leader is too far away, get closer to the leader
		if (leaderDist > Constants.TURRET_LEADER_CLOSENESS || leaderLocation == null)
		{
			this.pathFinder.calculateRoute(3);
			int distance = rc.getLocation().distanceSquaredTo(pathFinder.destination());
			if (distance >= Constants.REROUTING_DISTANCE) {
				pathFinder.estimateRoute();
				pathFinder.moveAlongRoute();
				pathFinder.initializeRoute(pathFinder.destination());
			}
			else {
				pathFinder.moveAlongRoute();
			}		}
		// Otherwise decrowd
		else
			decrowd();
	}
	
	protected void handleSignals() {
		for (Signal s : rc.emptySignalQueue()) {
			if (rc.getTeam().equals(s.getTeam())) {
				if (rc.getID() == this.leaderID)
					this.leaderLoc = rc.getLocation();
			}
			else {
				MapLocation signalLoc = s.getLocation();
				if (this.inAttackRange(signalLoc))
					this.reportedEnemies.add(signalLoc);
			}
		}
	}
	
	protected boolean attackNearbyEnemies() throws GameActionException {
				int attackRange = type.attackRadiusSquared;
		RobotInfo[] enemiesWithinRange = rc.senseHostileRobots(rc.getLocation(), attackRange);
		int count = 0;
		for (RobotInfo info : enemiesWithinRange)
			if (info.location.distanceSquaredTo(rc.getLocation()) > Constants.TURRET_INNER_RANGE)
				enemiesWithinRange[count++] = info;
		RobotInfo[] enemiesWithinFire = new RobotInfo[count];
		System.arraycopy(enemiesWithinRange, 0, enemiesWithinFire, 0, count);
		if (enemiesWithinFire.length > 0)
		{
			MapLocation attackLoc = lowestHealth(enemiesWithinFire).location;
			rc.attackLocation(attackLoc);
			return true;
		}
		else if (!this.reportedEnemies.isEmpty())
		{
			MapLocation closestSignal = this.closest(reportedEnemies);
			if (this.inAttackRange(closestSignal))
				rc.attackLocation(closestSignal);
			return true;
		}
		else
			return false;
	}
	
	private boolean inAttackRange(MapLocation location) {
		double signalDist = location.distanceSquaredTo(rc.getLocation());
		return (signalDist > Constants.TURRET_INNER_RANGE && signalDist < rc.getType().attackRadiusSquared);
	}
}
