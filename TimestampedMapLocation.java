package team237;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class TimestampedMapLocation implements Comparable<TimestampedMapLocation> {

	public MapLocation mapLocation;
	public Integer roundNumber;
	
	public TimestampedMapLocation(Integer mapLoc, Integer timestamp, RobotController rc) {
		this(Bot.decodePosition(rc.getLocation(), mapLoc), timestamp);
	}
	
	public TimestampedMapLocation(Integer mapLoc, RobotController rc) {
		this(Bot.decodePosition(rc.getLocation(), mapLoc), rc);
	}
	
	public TimestampedMapLocation(MapLocation mapLoc, RobotController rc) {
		this(mapLoc, rc.getRoundNum());
	}
	
	public TimestampedMapLocation(MapLocation mapLoc, Integer roundNum) {
		this.mapLocation = mapLoc;
		this.roundNumber = roundNum;
	}

	@Override
	public int compareTo(TimestampedMapLocation o) {
		return -1*(this.roundNumber - o.roundNumber);
	}
	
	@Override
	public String toString() {
		return this.mapLocation.toString() + ": " + this.roundNumber;
	}
}
