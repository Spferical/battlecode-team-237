package team237;

import java.util.PriorityQueue;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.Signal;
import battlecode.common.Team;

public class ZombieCowboyScout extends BaseScout {

	public ZombieCowboyScout(RobotController rcont, Signal[] messages) {
		super(rcont, messages);
		this.initialFriendlyArchonLocations = rc.getInitialArchonLocations(oppTeam);
		state = State.ZOMBIE_HERDER;
		
		int acceptTransmission = -1;
		int transmissionNum = 0;
		for (Signal s : messages)
		{
			if (s.getTeam() == rc.getTeam() && s.getMessage() != null)
			{
				int part0 = s.getMessage()[0]; 
				int part1 = s.getMessage()[1];
				
				// If the leader hails us
				if (part0 == Constants.TRANSMISSION_START && part1 == rc.getID())
				{
					acceptTransmission = s.getID();
					this.leaderID = acceptTransmission;
					continue;
				}
				// If the transmission is complete
				else if (part0 == Constants.TRANSMISSION_END && part1 == rc.getID())
				{
					acceptTransmission = -1;
					transmissionNum++;
					continue;
				}
				
				// If we're receiving a message from the leader about the location of 
				// enemy archons or zombie dens.
				if (acceptTransmission == s.getID()) 
				{	
					if (transmissionNum == 0)
					{
						MapLocation mapLoc = decodePosition(s.getLocation(), part0);
						TimestampedMapLocation messagedLocation = new TimestampedMapLocation(mapLoc, part1);
						this.enemyArchons.add(messagedLocation);
					}
					else
					{
						if (part0 > 0)
							this.zombieDens.add(decodePosition(s.getLocation(), part0));
						if (part1 > 0)
							this.zombieDens.add(decodePosition(s.getLocation(), part1));
					}
				}
			}
		}
		
		MapLocation zombieDen = closest(this.zombieDens);
		//rc.setIndicatorString(0, "Heading to location " + zombieDen);
		this.pathFinder.initializeRoute(zombieDen);
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				//rc.setIndicatorString(0, ""+state);
				handleSignals();
				if (rc.isCoreReady())
				{
					if (rc.getHealth() == rc.getType().maxHealth)
					{
						// If we're pretty close to the zombie den
						if (rc.getLocation().distanceSquaredTo(this.pathFinder.destination()) < 25)
						{
							// And there aren't any zombies, we try a different zombie den
							if (rc.senseNearbyRobots(type.sensorRadiusSquared, Team.ZOMBIE).length == 0)
							{
								this.zombieDens.remove(this.pathFinder.destination());
								MapLocation closestZombieDen = closest(this.zombieDens);
								
								if (closestZombieDen == null)
								{
									if (rc.isCoreReady())
									{
										this.runTowardEnemy(rc.senseNearbyRobots());
									}
								}
								else
									this.pathFinder.initializeRoute(closestZombieDen);
								rc.setIndicatorString(0, "Heading to location " + pathFinder.destination());
							}
						}
						
						if (rc.isCoreReady())
						{
							this.pathFinder.calculateRoute(3);
							this.pathFinder.moveAlongRoute();
						}
						bounds.update(rc);
					}
					else
						this.runTowardEnemy(rc.senseNearbyRobots());
				}
				cleanPriorityQueue(this.enemyArchons);
				Clock.yield();
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	private void handleSignals() {
		for (Signal s : rc.emptySignalQueue())
		{
			if (s.getID() == this.leaderID)
			{
				this.leaderLoc = s.getLocation();
			}
			if (s.getTeam() == rc.getTeam())
			{
				if (s.getMessage() == null)
					return;
				if (s.getMessage()[0] == Constants.ZOMBIE_DEN_FOUND_CODE)
					this.zombieDens.add(decodePosition(s.getLocation(), s.getMessage()[1]));
				if (s.getMessage()[0] == Constants.ZOMBIE_DEN_DESTROYED_CODE)
					this.zombieDens.remove(decodePosition(s.getLocation(), s.getMessage()[1]));
			}
		}
	}
	
	/** Currently runs towards friendly archons instead of enemy archons for some reason **/
	private void runTowardEnemy(RobotInfo[] robots) throws GameActionException {
		double friendx = 0;
		double friendy = 0;
		double friendWeight = 2;
		int friends = 0;
		for (TimestampedMapLocation location : this.enemyArchons) {
			MapLocation loc = location.mapLocation;
			friendx += loc.x;
			friendy += loc.y;
			friends++;
		}
		if (friends > 0) {
			friendx /= friends;
			friendy /= friends;
		}
		double oppx = 0;
		double oppy = 0;
		double oppWeight = 2;
		int opps = 0;
		for (MapLocation location : initialFriendlyArchonLocations) {
			oppx += location.x;
			oppy += location.y;
			opps++;
		}
		if (this.leaderLoc != null) {
			oppx += leaderLoc.x;
			oppy += leaderLoc.y;
			opps++;
		}
		/*for (SightedRobot archon : friendlyArchons.values()) {
			MapLocation location = archon.info.location;
			oppx += location.x;
			oppy += location.y;
			opps++;
		}*/
		if (opps > 0) {
			oppx /= opps;
			oppy /= opps;
		}
		double zomx = 0;
		double zomy = 0;
		double zomWeight = 1;
		int zoms = 0;
		for (RobotInfo robot : robots) {
			if (robot.team == Team.ZOMBIE) {
				zomx += robot.location.x;
				zomy += robot.location.y;
				zoms++;
			}
		}
		if (zoms > 0) {
			zomx /= zoms;
			zomy /= zoms;
		}
		MapLocation curLoc = rc.getLocation();
		double curx = curLoc.x;
		double cury = curLoc.y;
		double fx = friendx - curx;
		double fy = friendy - cury;
		double fl = Math.sqrt(fx*fx+fy*fy);
		double ox = oppx - curx;
		double oy = oppy - cury;
		double ol = Math.sqrt(ox*ox+oy*oy);
		double zx = zomx - curx;
		double zy = zomy - cury;
		double zl = Math.sqrt(zx*zx+zy*zy);
		double diffx = (fx/fl) * friendWeight - (ox/ol) * oppWeight - (zx/zl) * zomWeight;
		double diffy = (fy/fl) * friendWeight - (oy/ol) * oppWeight - (zy/zl) * zomWeight;
		Direction dir = directionTo(diffx, diffy).opposite();
		moveToward(curLoc.add(dir));
	}

	private PriorityQueue<TimestampedMapLocation> enemyArchons = new PriorityQueue<>();
	private PriorityQueue<MapLocation> zombieDens = new PriorityQueue<>();
	private MapLocation[] initialFriendlyArchonLocations;
}
