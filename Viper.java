package team237;

import java.util.Arrays;
import java.util.List;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.Team;

public class Viper extends Bodyguard {

	public Viper(RobotController rc) {
		super(rc);
	}
	
	public void viperInfectAlly() throws GameActionException {
		int attackRadius = rc.getType().attackRadiusSquared;
		MapLocation myLoc = rc.getLocation();
		RobotInfo[] myTeam = rc.senseNearbyRobots(attackRadius, ourTeam);
		for (RobotInfo robot: myTeam) {
			if (robot.health < Constants.VIPER_INFECT_HEALTH) {
				if (closerToEnemy(robot)) {
					if (rc.isCoreReady()) {
						rc.attackLocation(robot.location);
					}
				}
			}
		}
	}
	
	public void viperInfectEnemy() throws GameActionException {
		int attackRadius = rc.getType().attackRadiusSquared;
		MapLocation myLoc = rc.getLocation();
		RobotInfo[] myTeam = rc.senseNearbyRobots(attackRadius, oppTeam);
		for (RobotInfo robot: myTeam) {
			if (closerToEnemy(robot)) {
				if (rc.isCoreReady() && rc.isWeaponReady()) {
					rc.attackLocation(robot.location);
				}
			}
		}
	}
	
	public void viperInfectItself() throws GameActionException {
		if (closerToEnemy(rc.senseRobotAtLocation(rc.getLocation()))) {
			if (rc.isCoreReady() && rc.isWeaponReady()) {
				if (rc.getHealth() < Constants.VIPER_INFECT_HEALTH) {
					rc.attackLocation(rc.getLocation());
				}
			}
		}
	}
	
	@Override
	protected boolean attackEnemy() throws GameActionException {
		int attackRange = type.attackRadiusSquared;
		List<RobotInfo> robotsInSightRange = Arrays.asList(rc.senseNearbyRobots());
		List<RobotInfo> robotsInFireRange = Arrays.asList(rc.senseNearbyRobots(attackRange));
		List<RobotInfo> enemiesWithinSight = this.filterBots(robotsInSightRange, oppTeam);
		List<RobotInfo> enemiesWithinFire = this.filterBots(robotsInFireRange, oppTeam);
		RobotInfo closestUnit = this.closest(robotsInSightRange);
		List<RobotInfo> hostilesInSight = this.filterBots(robotsInSightRange, rc.getTeam(), true);
		List<RobotInfo> hostilesInFire = this.filterBots(robotsInFireRange, rc.getTeam(), true);
		if (hostilesInSight.size() > 0) {
			if (hostilesInFire.size() > 0) {
				// suicide if health is low;
				if (closestUnit.team != rc.getTeam())
					viperInfectItself();
				
				// If there's no enemies, attack zombies
				if (enemiesWithinSight.size() == 0)
					enemiesWithinSight.addAll(hostilesInSight);
				if (enemiesWithinFire.size() == 0)
					enemiesWithinFire.addAll(hostilesInFire);
				
				// If there's no enemies, decrowd
				if (enemiesWithinFire.size() == 0)
				{
					this.decrowd();
					return true;
				}
				
				// regular attack
				RobotInfo attackRobot = lowestHealth(enemiesWithinFire);
				MapLocation attackLoc = attackRobot.location;
				if (attackRobot.type.canAttack()) {
					if (rc.getType().attackRadiusSquared > (attackLoc.distanceSquaredTo(rc.getLocation()) * 3) && rc.isCoreReady()) {
						moveFrom(attackLoc);
					}
					else if (rc.getLocation().distanceSquaredTo(closest(enemiesWithinFire).location) 
							<= Constants.RUN_AWAY_DISTANCE_SQAURED && rc.isCoreReady() 
							&& rc.getType().attackRadiusSquared > Constants.RUN_AWAY_DISTANCE_SQAURED) {
						moveFrom(closest(enemiesWithinFire).location);
					}
				}
				if (rc.isWeaponReady() && rc.isCoreReady()) {
					if (attackRobot.team == Team.ZOMBIE) {
						rc.attackLocation(attackLoc);
					} else	viperInfectEnemy();
				}
			} else if (rc.isWeaponReady() && rc.isCoreReady()) {
				viperInfectAlly(); // if possible and necessary
			} else if (rc.isCoreReady()) {
				moveToward(lowestHealth(enemiesWithinSight).location);
			}
			return true;
		}
		return false;
	}
}
